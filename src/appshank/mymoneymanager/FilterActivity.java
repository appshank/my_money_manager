package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import appshank.mymoneymanager.Category.ExpenseCategory;
import appshank.mymoneymanager.Category.IncomeCategory;

import com.google.analytics.tracking.android.EasyTracker;

public class FilterActivity extends Activity {

	// reference to the fields
	EditText editTextRemarksContains;
	EditText editTextMinAmount;
	EditText editTextMaxAmount;
	CheckBox checkBoxIncome;
	CheckBox checkBoxExpense;
	Button buttonSelectedCategories;
	Button buttonIncomes;
	Button buttonExpenses;
	LinearLayout selectedCategoriesLayout;
	TextView textViewCategoryCount;

	View expensesCategoryLayout;
	View incomesCategoryLayout;

	boolean allCategoriesSelected;
	boolean allIncomesSelected;
	boolean allExpensesSelected;

	List<String> expenseCategoryStrings;
	List<String> incomeCategoryStrings;

	List<String> selectedCategoryStrings;
	List<String> selectedExpenseCategoryStrings;
	List<String> selectedIncomeCategoryStrings;

	ExpenseCategory[] expenseCategories;
	IncomeCategory[] incomeCategories;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_filter);


		// initialize references to the fields
		editTextRemarksContains = (EditText) findViewById(R.id.editText_remarks_contains);
		editTextMinAmount = (EditText) findViewById(R.id.editText_min_amount);
		editTextMaxAmount = (EditText) findViewById(R.id.editText_max_amount);
		checkBoxIncome = (CheckBox) findViewById(R.id.checkBox_income);
		checkBoxExpense = (CheckBox) findViewById(R.id.checkBox_expense);
		buttonSelectedCategories = (Button) findViewById(R.id.button_selected_categories);
		buttonIncomes = (Button) findViewById(R.id.button_incomes);
		buttonExpenses = (Button) findViewById(R.id.button_expenses);
		selectedCategoriesLayout = (LinearLayout) findViewById(R.id.layout_selected_categories);
		textViewCategoryCount = (TextView) findViewById(R.id.textView_category_count);

		expensesCategoryLayout = findViewById(R.id.layout_all_expenses_categories);
		incomesCategoryLayout = findViewById(R.id.layout_all_incomes_categories);

		// setup the UI to remove the soft keyboard when the user click outside
		// of edit text etc
		Utils.setupUI(findViewById(R.id.layout_filters), this);

		// add listeners to checkboxes
		setupListenersOnCheckboxes();

		// cache the income and expense categories
		expenseCategories = ExpenseCategory.values();
		incomeCategories = IncomeCategory.values();

		// cache the income and expense category strings
		expenseCategoryStrings = CategoryUtils
				.getCategoryStrings(ExpenseCategory.class);
		incomeCategoryStrings = CategoryUtils
				.getCategoryStrings(IncomeCategory.class);

		// initilalize the categoryStrings
		selectedExpenseCategoryStrings = new ArrayList<String>();
		selectedIncomeCategoryStrings = new ArrayList<String>();
		selectedCategoryStrings = new ArrayList<String>();

		// category count
		updateCategoryCountTextView();

		// set the allCategoriesSelected flag to true
		allCategoriesSelected = true;

		// set the visibility of individual category layouts to gone
		Utils.setVisibility(incomesCategoryLayout, false);
		Utils.setVisibility(expensesCategoryLayout, false);

		// set the default selected drawables
		// add and display the selected categories to the selected categories
		// scrollview and respective layouts
		if (checkBoxIncome.isChecked()) {
			allIncomesSelected = true;
			selectAllExpenses();
		}
		if (checkBoxExpense.isChecked()) {
			allExpensesSelected = true;
			selectAllIncomes();
		}
	}

	private void setupListenersOnCheckboxes() {

		// Expense checkbox
		checkBoxExpense
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Log.d("FilterActivity.setupListenersOnCheckboxes",
								"checkBoxExpense state changed to "
										+ (isChecked ? "checked" : "unchecked"));

						if (isChecked) {
							// display the expense categories layout
							Utils.setVisibility(expensesCategoryLayout, true);
						} else {
							// deselect all expense categories
							deselectAllExpenses();

							// hide the expense categories layout
							Utils.setVisibility(expensesCategoryLayout, false);
						}
					}
				});

		// Income checkbox
		checkBoxIncome
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Log.d("FilterActivity.setupListenersOnCheckboxes",
								"checkBoxIncome state changed to "
										+ (isChecked ? "checked" : "unchecked"));

						if (isChecked) {
							// display the expense categories layout
							Utils.setVisibility(incomesCategoryLayout, true);
						} else {
							// deselect all expense categories
							deselectAllIncomes();

							// hide the expense categories layout
							Utils.setVisibility(incomesCategoryLayout, false);
						}
					}
				});
	}

	private void updateCategoryCountTextView() {
		textViewCategoryCount.setText("(" + selectedCategoryStrings.size()
				+ ")");
	}

	private void addToSelectedCategoriesScrollView(String selectedCategory) {
		// get the category corresponding to this string
		final Enum category = CategoryUtils
				.getCategoryFromString(selectedCategory);

		// get the set drawable corresponding to this category
		int drawableId = CategoryUtils.getCategorySetDrawable(category);

		// add an imageview to the scrollview
		ImageView imageView = new ImageView(this);

		// set the drawable for this imageview to the correct set drawable found
		// earlier
		imageView.setImageResource(drawableId);
		imageView.setTag(category.name());
		imageView.setAdjustViewBounds(true);
		int padding = Utils.getDPToPixels(this, 4);
		Log.d("FilterActivity.addToSelectedCategoriesScrollView()",
				"padding in pixels : " + padding);
		imageView.setPadding(padding, padding, padding, padding);
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageView correspondingImageView = CategoryUtils
						.getImageViewForCategory(FilterActivity.this, category);
				onClickCategory(correspondingImageView);
			}
		});

		int dipAsPerLayoutSize = 50;
		if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
			dipAsPerLayoutSize = 62;
		}

		int height = Utils.getDPToPixels(this, dipAsPerLayoutSize);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				height);
		selectedCategoriesLayout.addView(imageView, layoutParams);

		Log.d("FilterActivity.addToSelectedCategoriesScrollView()",
				"added category " + category.name() + " to scrollView");

	}

	@Override
	protected void onStart() {
		super.onStart();

		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_filter, menu);
		return true;
	}

	public void onClickDone(View view) {
		// get the values from the fields
		String remarksContains = editTextRemarksContains.getText().toString();
		String minAmountString = editTextMinAmount.getText().toString();
		String maxAmountString = editTextMaxAmount.getText().toString();

		// if the values are not empty, insert in the intent and pass back
		// along with result as data
		int resultCode = Constants.RESULT_CODE_FILTERS_NOT_SET;
		Intent intent = getIntent();

		ReportFilters filters = new ReportFilters();

		// remarks
		if (remarksContains != null && remarksContains != ""
				&& remarksContains.length() != 0 && remarksContains != " ") {
			filters.setRemarksContains(remarksContains);
			resultCode = Constants.RESULT_CODE_FILTERS_SET;
		}

		// min amount
		if (minAmountString != null && minAmountString != ""
				&& minAmountString.length() != 0) {
			try {
				float minAmount = Float.parseFloat(minAmountString);
				filters.setMinAmount(minAmount);
				resultCode = Constants.RESULT_CODE_FILTERS_SET;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		// max amount
		if (maxAmountString != null && maxAmountString != ""
				&& maxAmountString.length() != 0) {
			try {
				float maxAmount = Float.parseFloat(maxAmountString);
				filters.setMaxAmount(maxAmount);
				resultCode = Constants.RESULT_CODE_FILTERS_SET;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		// income categories
		// expense categories
		filters.setCategories(selectedCategoryStrings);
		if (selectedCategoryStrings.size() != expenseCategoryStrings.size()
				+ incomeCategoryStrings.size()) {
			// only set the categories on the filters in case the user has made
			// any modifications to the default categories
			resultCode = Constants.RESULT_CODE_FILTERS_SET;
		}

		if (resultCode == Constants.RESULT_CODE_FILTERS_SET) {
			intent.putExtra("filters", filters);
		}

		setResult(resultCode, intent);

		// display the toast according to the resultCode
		Utils.startToastService(this, Utils.getToastText(resultCode));

		finish();
	}

	public void onClickSelectedCategories(View view) {
		// display the categories layout as per the checkboxes state
		displayCategoriesLayoutsAsPerCheckboxes();

		if (buttonSelectedCategories.getText().equals("Deselect All")) {
			// if control reaches here that the button was in
			// allCategoriesSelected state and the text was Deselect All

			// change text on button to select all categories
			buttonSelectedCategories.setText("Select All");

			// deselect all expenses
			deselectAllExpenses();

			// deselect all incomes
			deselectAllIncomes();
		} else {
			// if control reaches here that the button was in
			// allCategories deselected state and the text was select all

			// change text on button to deselect all categories
			buttonSelectedCategories.setText("Deselect All");

			if (checkBoxExpense.isChecked()) {
				// deselect all expenses
				selectAllExpenses();
			}

			if (checkBoxIncome.isChecked()) {
				// deselect all incomes
				selectAllIncomes();
			}
		}

	}

	private void deselectAllIncomes() {
		// change the text on the button
		buttonIncomes.setText("Select All");

		// remove all the expense categories from selected Categories list
		removeAllIncomes();
	}

	private void removeAllIncomes() {
		// iterate over the expense categories
		List<String> tmpList = new ArrayList<String>(
				selectedIncomeCategoryStrings);

		for (String incomeCategoryString : tmpList) {
			deselectIncomeCategory(incomeCategoryString);
		}
	}

	private void deselectIncomeCategory(String incomeCategoryString) {
		Log.d("FilterActivity.deselectIncomeCategory()",
				"deselecting income category " + incomeCategoryString);

		// remove from the lists
		selectedIncomeCategoryStrings.remove(incomeCategoryString);
		selectedCategoryStrings.remove(incomeCategoryString);

		// remove corresponding category from scrollview
		removeFromSelectedCategoriesScrollView(incomeCategoryString);

		// reset drawable for this category
		resetDrawableForCategory(incomeCategoryString);

		// update the category count
		updateCategoryCountTextView();
	}

	private void resetDrawableForCategory(String categoryStringToBeRemoved) {
		Enum category = CategoryUtils
				.getCategoryFromString(categoryStringToBeRemoved);

		ImageView categoryImageView = CategoryUtils.getImageViewForCategory(
				this, category);

		categoryImageView.setImageResource(CategoryUtils
				.getCategoryResetDrawable(category));
	}

	private void setDrawableForCategory(String categoryStringToBeAdded) {
		Enum category = CategoryUtils
				.getCategoryFromString(categoryStringToBeAdded);

		ImageView categoryImageView = CategoryUtils.getImageViewForCategory(
				this, category);

		categoryImageView.setImageResource(CategoryUtils
				.getCategorySetDrawable(category));
	}

	private void removeChildViewAsPerTag(LinearLayout selectedCategoriesLayout,
			String categoryStringToBeRemoved) {

		// find the child of scroll view which has the tag as this
		// drawable
		int childCount = selectedCategoriesLayout.getChildCount();
		for (int i = 0; i < childCount; i++) {
			ImageView categoryImageView = (ImageView) selectedCategoriesLayout
					.getChildAt(i);
			if (categoryImageView.getTag().equals(categoryStringToBeRemoved)) {
				selectedCategoriesLayout.removeView(categoryImageView);
				return;
			}
		}
	}

	private void displayCategoriesLayoutsAsPerCheckboxes() {
		if (checkBoxIncome.isChecked())
			Utils.setVisibility(incomesCategoryLayout, true);
		if (checkBoxExpense.isChecked())
			Utils.setVisibility(expensesCategoryLayout, true);
	}

	public void onClickExpenses(View view) {
		if (buttonExpenses.getText().equals("Deselect All")) {
			// if control reaches here than the previous text was "Deselect All"
			deselectAllExpenses();
		} else {
			// if control reaches here than the previous text was "Select All"
			selectAllExpenses();
		}
	}

	private void selectAllExpenses() {
		Log.d("FilterActivity.selectAllExpenses()", "selecting all expenses...");

		// change the text on the button
		buttonExpenses.setText("Deselect All");

		// add all the expense categories
		addAllExpenses();
	}

	private void selectAllIncomes() {
		Log.d("FilterActivity.selectAllIncomes()", "selecting all incomes...");

		// change the text on the button
		buttonIncomes.setText("Deselect All");

		// add all the expense categories
		addAllIncomes();

	}

	private void addAllIncomes() {
		// // // iterate over the expense categories
		List<String> tmpList = new ArrayList<String>(incomeCategoryStrings);

		for (String incomeCategoryString : tmpList) {
			// check if its not already selected
			if (!selectedIncomeCategoryStrings.contains(incomeCategoryString)) {
				selectIncomeCategory(incomeCategoryString);
				updateCategoryCountTextView();
			}
		}
	}

	private void selectIncomeCategory(String incomeCategoryString) {
		// add to the lists
		selectedIncomeCategoryStrings.add(incomeCategoryString);
		selectedCategoryStrings.add(incomeCategoryString);

		// add corresponding category to scrollview
		addToSelectedCategoriesScrollView(incomeCategoryString);

		// set drawable for this category
		setDrawableForCategory(incomeCategoryString);

		// update the category count
		updateCategoryCountTextView();
	}

	private void addAllExpenses() {
		// // iterate over the expense categories
		List<String> tmpList = new ArrayList<String>(expenseCategoryStrings);

		for (String expenseCategoryString : tmpList) {
			// check if its not already selected
			if (!selectedExpenseCategoryStrings.contains(expenseCategoryString)) {
				selectExpenseCategory(expenseCategoryString);
				updateCategoryCountTextView();
			}
		}
	}

	private void selectExpenseCategory(String expenseCategoryString) {
		// add to the lists
		selectedExpenseCategoryStrings.add(expenseCategoryString);
		selectedCategoryStrings.add(expenseCategoryString);

		// add corresponding category to scrollview
		addToSelectedCategoriesScrollView(expenseCategoryString);

		// set drawable for this category
		setDrawableForCategory(expenseCategoryString);

		// update the category count
		updateCategoryCountTextView();
	}

	private void deselectAllExpenses() {
		// change the text on the button
		buttonExpenses.setText("Select All");

		// remove all the expense categories from selected Categories list
		removeAllExpenses();
	}

	private void removeAllExpenses() {
		// iterate over the expense categories
		List<String> tmpList = new ArrayList<String>(
				selectedExpenseCategoryStrings);

		for (String expenseCategoryString : tmpList) {
			deselectExpenseCategory(expenseCategoryString);
		}
	}

	private void deselectExpenseCategory(String expenseCategoryString) {
		Log.d("FilterActivity.deselectExpenseCategory()",
				"deselecting expense category " + expenseCategoryString);

		// remove from the lists
		selectedExpenseCategoryStrings.remove(expenseCategoryString);
		selectedCategoryStrings.remove(expenseCategoryString);

		// remove corresponding category from scrollview
		removeFromSelectedCategoriesScrollView(expenseCategoryString);

		// set drawable for this category
		resetDrawableForCategory(expenseCategoryString);

		// update the category count
		updateCategoryCountTextView();
	}

	private void removeFromSelectedCategoriesScrollView(
			String categoryStringToBeRemoved) {
		// remove the image view corresponding to this category from the
		// selected categories scroll view
		removeChildViewAsPerTag(selectedCategoriesLayout,
				categoryStringToBeRemoved);
	}

	public void onClickIncomes(View view) {
		if (buttonIncomes.getText().equals("Deselect All")) {
			// if control reaches here than the previous text was "Deselect All"
			deselectAllIncomes();
		} else {
			// if control reaches here than the previous text was "Select All"
			selectAllIncomes();
		}
	}

	public void onClickCategory(View view) {
		Log.d("FilterActivity.onClickCategory()", "view: " + view);

		Enum category = CategoryUtils
				.getCategoryFromImageView((ImageView) view);

		// if this category was already selected
		List<String> tmpList = new ArrayList<String>(selectedCategoryStrings);

		Class categoryClass = CategoryUtils.getCategoryType(category);
		Log.d("FilterActivity.onClickCategory()", "categoryClass : "
				+ categoryClass);

		if (tmpList.contains(category.name())) {
			// deselect the category

			if (categoryClass.equals(ExpenseCategory.class)) {
				Log.d("FilterActivity.onClickCategory()",
						"deselecting expense category " + category.name());
				deselectExpenseCategory(category.name());
				return;
			} else if (categoryClass.equals(IncomeCategory.class)) {
				Log.d("FilterActivity.onClickCategory()",
						"deselecting income category " + category.name());
				deselectIncomeCategory(category.name());
				return;
			}
			throw new IllegalStateException("unknown category type !");
		} else {
			// if the control reaches here than the category was not previously
			// selected

			if (categoryClass.equals(ExpenseCategory.class)) {
				Log.d("FilterActivity.onClickCategory()",
						"selecting expense category " + category.name());
				selectExpenseCategory(category.name());
				return;
			} else if (categoryClass.equals(IncomeCategory.class)) {
				Log.d("FilterActivity.onClickCategory()",
						"selecting income category " + category.name());
				selectIncomeCategory(category.name());
				return;
			}
			throw new IllegalStateException("unknown category type !");
		}

	}

	@Override
	protected void onStop() {
		super.onStop();

		EasyTracker.getInstance().activityStop(this);
	}

}
