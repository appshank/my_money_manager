package appshank.mymoneymanager;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

public class XLSReportGenerator implements ReportGenerator {

	// cursor for the data from which the report is to be generated
	Cursor cursor;

	public XLSReportGenerator(Cursor cursor) {
		this.cursor = cursor;
	}

	@Override
	public String generateReport() {
		// generate the report using cursor
		// int nrows = cursor.getCount();
		int ncols = cursor.getColumnCount();

		// iterate over the elements in the Cursor
		if (cursor.moveToFirst()) {

			// create title
			String fileName = createTitle();

			// create the workbook
			WritableWorkbook reportWb = createWorkbook(fileName);

			// create the sheet
			WritableSheet reportSheet = createSheet(reportWb, "Transactions", 0);

			// initialize the column indexes (as per database table)
			final int dateColumnIndex = cursor
					.getColumnIndex(TransactionsDatabaseHelper.COLUMN_DATE);
			final int typeColumnIndex = cursor
					.getColumnIndex(TransactionsDatabaseHelper.COLUMN_TRANSACTION_TYPE);
			final int amountColumnIndex = cursor
					.getColumnIndex(TransactionsDatabaseHelper.COLUMN_AMOUNT);
			final int categoryColumnIndex = cursor
					.getColumnIndex(TransactionsDatabaseHelper.COLUMN_CATEGORY);
			final int remarksColumnIndex = cursor
					.getColumnIndex(TransactionsDatabaseHelper.COLUMN_REMARKS);

			// the format of the xls is -> S.No. - date - type - amount -
			// category - remarks
			final int snoCol = 0;
			final int dateCol = 1;
			final int typeCol = 2;
			final int amountCol = 3;
			final int categoryCol = 4;
			final int remarksCol = 5;

			int sno = 1;

			// insert records in individual rows in the

			try {

				// create the header column
				writeLabelCell(snoCol, 0, "S.No.", true, reportSheet);
				writeLabelCell(dateCol, 0, "Date", true, reportSheet);
				writeLabelCell(typeCol, 0, "Type", true, reportSheet);
				writeLabelCell(amountCol, 0, "Amount", true, reportSheet);
				writeLabelCell(categoryCol, 0, "Category", true, reportSheet);
				writeLabelCell(remarksCol, 0, "Remarks", true, reportSheet);

				do {

					// iterate over each column
					for (int colNum = 0; colNum < ncols; colNum++) {
						Log.d(getClass().getSimpleName(), "colNum: " + colNum);
						switch (colNum) {
						case snoCol:
							writeNumberCell(snoCol, sno, sno, false,
									reportSheet);
							break;

						case dateCol:
							long dateTime = cursor.getLong(dateColumnIndex);
							writeDateCell(dateCol, sno, new Date(dateTime),
									false, reportSheet);
							break;

						case typeCol:
							String type = cursor.getString(typeColumnIndex);
							writeLabelCell(typeCol, sno,
									type == Transaction.TYPE_INCOME ? "Income"
											: "Expense", false, reportSheet);
							break;

						case amountCol:
							float amount = cursor.getFloat(amountColumnIndex);
							writeNumberCell(amountCol, sno, amount, false,
									reportSheet);
							break;

						case categoryCol:
							String category = cursor
									.getString(categoryColumnIndex);
							writeLabelCell(categoryCol, sno, category, false,
									reportSheet);
							break;

						case remarksCol:
							String remarks = cursor
									.getString(remarksColumnIndex);
							writeLabelCell(remarksCol, sno, remarks, false,
									reportSheet);
							break;
						}
					}

					// increase the sno (row number)
					sno += 1;
					// move to next record
				} while (cursor.moveToNext());

				// adjust cell alignments
				CellView tmpCell = null;

				// sno as per header - row 0, col 0
				tmpCell = reportSheet.getColumnView(snoCol);
				tmpCell.setAutosize(true);
				reportSheet.setColumnView(snoCol, tmpCell);

				// date as per second date and add 2 to it - row 1, col 1
				tmpCell = reportSheet.getColumnView(dateCol);
				tmpCell.setAutosize(true);
				reportSheet.setColumnView(dateCol, tmpCell);

				// Type as per header - row 0, col 2
				tmpCell = reportSheet.getColumnView(typeCol);
				tmpCell.setAutosize(true);
				reportSheet.setColumnView(typeCol, tmpCell);

				// amount as per header - row 0, col 3
				tmpCell = reportSheet.getColumnView(amountCol);
				tmpCell.setAutosize(true);
				reportSheet.setColumnView(amountCol, tmpCell);

				// Category as per header - row 0, col 5
				tmpCell = reportSheet.getColumnView(categoryCol);
				tmpCell.setAutosize(true);

				// Remarks as per header*3 - row 0, col 5
				tmpCell = reportSheet.getColumnView(remarksCol);
				tmpCell.setAutosize(true);
				reportSheet.setColumnView(remarksCol, tmpCell);

				// write the date to the file and close it
				reportWb.write();
				reportWb.close();
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String reportFilePath = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/MyMoneyManager/" + fileName;

			return reportFilePath;

		}

		return null;
	}

	private String createTitle() {
		StringBuilder titleBuilder = new StringBuilder();
		titleBuilder.append("Transactions-");
		titleBuilder.append(dateString());
		titleBuilder.append("-");

		// move cursor to last element to get the date of the last transaction
		cursor.moveToLast();
		titleBuilder.append(dateString());
		titleBuilder.append(".xls");

		// reset the cursor's position
		cursor.moveToFirst();
		return titleBuilder.toString();
	}

	private StringBuilder dateString() {
		long startDateTime = cursor.getLong((cursor
				.getColumnIndex(TransactionsDatabaseHelper.COLUMN_DATE)));
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(startDateTime);
		StringBuilder dateBuilder = new StringBuilder();
		dateBuilder.append(c.get(Calendar.DAY_OF_MONTH));
		// dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.MONTH) + 1);
		// dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.YEAR));
		return dateBuilder;
	}

	/**
	 * 
	 * @param fileName
	 *            - the name to give the new workbook file
	 * @return - a new WritableWorkbook with the given fileName
	 */
	public WritableWorkbook createWorkbook(String fileName) {
		// exports must use a temp file while writing to avoid memory hogging
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setUseTemporaryFileDuringWrite(true);

		// get the sdcard's directory
		File sdCard = Environment.getExternalStorageDirectory();
		// add on the your app's path
		File dir = new File(sdCard.getAbsolutePath() + "/MyMoneyManager");
		// make them in case they're not there
		dir.mkdirs();

		checkAndDeleteOldTransaction(fileName, dir);

		// create a standard java.io.File object for the Workbook to use
		File wbfile = new File(dir, fileName);

		WritableWorkbook wb = null;

		try {
			// create a new WritableWorkbook using the java.io.File and
			// WorkbookSettings from above
			wb = Workbook.createWorkbook(wbfile, wbSettings);
		} catch (IOException ex) {
			Log.e("XLSReportGenerator.createWorkbook()", ex.getStackTrace()
					.toString());
			Log.e("XLSReportGenerator.createWorkbook()", ex.getMessage());
		}

		return wb;
	}

	private void checkAndDeleteOldTransaction(String fileName, File dir) {
		String[] existingReportFiles = dir.list();

		if (existingReportFiles == null)
			return;

		int numOfFiles = existingReportFiles.length;
		boolean exists = false;
		for (int i = 0; i < numOfFiles; i++) {
			if (existingReportFiles[i].equals(fileName)) {
				exists = true;
				break;
			}
		}
		if (exists) {
			File oldWbFile = new File(dir, fileName);
			oldWbFile.delete();
		}
	}

	/**
	 * 
	 * @param wb
	 *            - WritableWorkbook to create new sheet in
	 * @param sheetName
	 *            - name to be given to new sheet
	 * @param sheetIndex
	 *            - position in sheet tabs at bottom of workbook
	 * @return - a new WritableSheet in given WritableWorkbook
	 */
	public WritableSheet createSheet(WritableWorkbook wb, String sheetName,
			int sheetIndex) {
		// create a new WritableSheet and return it
		return wb.createSheet(sheetName, sheetIndex);
	}

	/**
	 * 
	 * @param columnPosition
	 *            - column to place new cell in
	 * @param rowPosition
	 *            - row to place new cell in
	 * @param contents
	 *            - string value to place in cell
	 * @param headerCell
	 *            - whether to give this cell special formatting
	 * @param sheet
	 *            - WritableSheet to place cell in
	 * @throws RowsExceededException
	 *             - thrown if adding cell exceeds .xls row limit
	 * @throws WriteException
	 *             - Idunno, might be thrown
	 */
	public void writeLabelCell(int columnPosition, int rowPosition,
			String contents, boolean headerCell, WritableSheet sheet)
			throws RowsExceededException, WriteException {
		// create a new cell with contents at position
		Label newCell = new Label(columnPosition, rowPosition, contents);

		setCellAlignment(newCell, Alignment.CENTRE);

		if (headerCell) {
			// give header cells size 10 Arial bolded
			WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10,
					WritableFont.BOLD);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			// center align the cells' contents
			headerFormat.setAlignment(Alignment.CENTRE);
			newCell.setCellFormat(headerFormat);
		}

		sheet.addCell(newCell);
	}

	private void setCellAlignment(WritableCell newCell, Alignment alignment)
			throws WriteException {
		WritableCellFormat cellFormat = null;

		if (newCell.getClass().equals(DateTime.class)) {
			DateFormat dateFormat = new DateFormat("dd/mm/yyyy");
			cellFormat = new WritableCellFormat(dateFormat);
		} else
			cellFormat = new WritableCellFormat();

		cellFormat.setAlignment(alignment);
		newCell.setCellFormat(cellFormat);
	}

	public void writeDateCell(int columnPosition, int rowPosition, Date date,
			boolean headerCell, WritableSheet sheet)
			throws RowsExceededException, WriteException {
		// create a new cell with contents at position
		DateTime newCell = new DateTime(columnPosition, rowPosition, date);

		setCellAlignment(newCell, Alignment.CENTRE);

		if (headerCell) {
			// give header cells size 10 Arial bolded
			WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10,
					WritableFont.BOLD);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			// center align the cells' contents
			headerFormat.setAlignment(Alignment.CENTRE);
			newCell.setCellFormat(headerFormat);
		}

		sheet.addCell(newCell);
	}

	public void writeNumberCell(int columnPosition, int rowPosition,
			double amount, boolean headerCell, WritableSheet sheet)
			throws RowsExceededException, WriteException {
		// create a new cell with contents at position
		Number newCell = new Number(columnPosition, rowPosition, amount);

		setCellAlignment(newCell, Alignment.RIGHT);

		if (headerCell) {
			// give header cells size 10 Arial bolded
			WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10,
					WritableFont.BOLD);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			// center align the cells' contents
			headerFormat.setAlignment(Alignment.CENTRE);
			newCell.setCellFormat(headerFormat);
		}

		sheet.addCell(newCell);
	}
}
