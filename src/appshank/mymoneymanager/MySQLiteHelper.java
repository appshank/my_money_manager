package appshank.mymoneymanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	private static MySQLiteHelper dbHelperInstance = null;

	public static final String DB_NAME = "MyTransactionsDB";
	public static final int DB_VERSION = 4;

	public static final String TABLE_TRANSACTIONS = "MyTransactions";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TRANSACTION_TYPE = "type";
	public static final String COLUMN_AMOUNT = "amount";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_REMARKS = "remarks";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_BILL = "bill";

	private static final String DB_CREATE = "create table if not exists "
			+ TABLE_TRANSACTIONS + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_DATE
			+ " integer, " + COLUMN_TRANSACTION_TYPE + " varchar, "
			+ COLUMN_AMOUNT + " real, " + COLUMN_CATEGORY + " varchar, "
			+ COLUMN_REMARKS + " varchar);";

	private MySQLiteHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	public static MySQLiteHelper getInstance(Context ctx) {
		// Use the application context, which will ensure that you
		// don't accidentally leak an Activity's context.
		// See this article for more information: http://bit.ly/6LRzfx
		if (dbHelperInstance == null) {
			dbHelperInstance = new MySQLiteHelper(ctx.getApplicationContext());
		}
		return dbHelperInstance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTIONS);
		onCreate(db);
	}

	@Override
	public synchronized void close() {
		Log.d(getClass().getSimpleName(), "closing database..");
		super.close();
	}

	@Override
	public SQLiteDatabase getWritableDatabase() {
		Log.d(getClass().getSimpleName(), "opening database..");
		return super.getWritableDatabase();
	}

}