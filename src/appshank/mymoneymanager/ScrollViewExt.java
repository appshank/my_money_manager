package appshank.mymoneymanager;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;

public class ScrollViewExt extends HorizontalScrollView {

	public ScrollViewExt(Context context) {
		super(context);
	}

	public ScrollViewExt(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ScrollViewExt(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		Log.d("ScrollViewExt.onScrollChanged()",
				"inside overridden onScrollChanged()");

		// Grab the last child placed in the ScrollView, we need it to
		// determinate the bottom position.
		View view = (View) getChildAt(getChildCount() - 1);

		// Calculate the scrolldiff
		Log.d("ScrollViewExt.onScrollChanged()",
				"view.getRight() : " + view.getRight());
		Log.d("ScrollViewExt.onScrollChanged()", "getWidth() : " + getWidth());
		Log.d("ScrollViewExt.onScrollChanged()", "getScrollX() : "
				+ getScrollX());
		int diff = (view.getRight() - (getWidth() + getScrollX()));

		// if diff is zero, then the bottom has been reached
		if (diff <= 0) {
			// notify that we have reached the bottom
			Log.d("ScrollViewExt.onScrollChanged()",
					"MyScrollView: Bottom has been reached");
		}

		super.onScrollChanged(l, t, oldl, oldt);
	}

}
