package appshank.mymoneymanager;

import java.text.NumberFormat;

/**
 * class that stores the various settings in the form of static variables. This
 * can also be considered as the GLOBALS class
 * 
 * @author AppShank
 * 
 */
public class Settings {

	public static int START_DATE_OF_MONTH = 1;

	public static String CURRENCY_SYMBOL;

	public static NumberFormat format;
}
