package appshank.mymoneymanager;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

@ReportsCrashes(formKey = "", // will not be used
mailTo = "theappshank@gmail.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_report_text)
public class MyMoneyManagerApplication extends Application {

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		// initialize ARCA
		ACRA.init(this);
	}
}
