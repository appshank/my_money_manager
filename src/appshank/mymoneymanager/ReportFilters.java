package appshank.mymoneymanager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ReportFilters implements Serializable {

	/**
	 * generate serialVersionUID
	 */
	private static final long serialVersionUID = 8322006874209292424L;

	private Date startDate;
	private Date endDate;

	private boolean remarksContainsSetFlag;
	private String remarksContains;

	private boolean minAmountSetFlag;
	private float minAmount;

	private boolean maxAmountSetFlag;
	private float maxAmount;

	// we dont need a flag for categories as categories will always be there
	private List<String> categories;

	public ReportFilters() {
		startDate = new Date();
		endDate = new Date();
		remarksContainsSetFlag = false;
		minAmountSetFlag = false;
		maxAmountSetFlag = false;
	}

	public ReportFilters setRemarksContains(String remarksContains) {
		remarksContainsSetFlag = true;
		this.remarksContains = remarksContains;
		return this;
	}

	public ReportFilters setMinAmount(float minAmount) {
		minAmountSetFlag = true;
		this.minAmount = minAmount;
		return this;
	}

	public ReportFilters setMaxAmount(float maxAmount) {
		maxAmountSetFlag = true;
		this.maxAmount = maxAmount;
		return this;
	}

	public ReportFilters setDates(Date startDate, Date endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
		return this;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getRemarksContains() {
		return remarksContains;
	}

	public float getMinAmount() {
		return minAmount;
	}

	public float getMaxAmount() {
		return maxAmount;
	}

	public List<String> getCategories() {
		return categories;
	}

	public boolean isRemarksContainsSetFlag() {
		return remarksContainsSetFlag;
	}

	public boolean isMinAmountSetFlag() {
		return minAmountSetFlag;
	}

	public boolean isMaxAmountSetFlag() {
		return maxAmountSetFlag;
	}

}
