package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class TransactionsDatabaseHelper {
	public static final String DB_NAME = MySQLiteHelper.DB_NAME;
	public static final int DB_VERSION = MySQLiteHelper.DB_VERSION;

	public static final String TABLE_TRANSACTIONS = MySQLiteHelper.TABLE_TRANSACTIONS;
	public static final String COLUMN_ID = MySQLiteHelper.COLUMN_ID;
	public static final String COLUMN_TRANSACTION_TYPE = MySQLiteHelper.COLUMN_TRANSACTION_TYPE;
	public static final String COLUMN_AMOUNT = MySQLiteHelper.COLUMN_AMOUNT;
	public static final String COLUMN_DATE = MySQLiteHelper.COLUMN_DATE;
	public static final String COLUMN_REMARKS = MySQLiteHelper.COLUMN_REMARKS;
	public static final String COLUMN_CATEGORY = MySQLiteHelper.COLUMN_CATEGORY;
	public static final String COLUMN_BILL = MySQLiteHelper.COLUMN_BILL;

	/*
	 * private static SimpleDateFormat dateFormat = new SimpleDateFormat(
	 * "yyyy-mm-dd");
	 */
	private SQLiteDatabase db;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { COLUMN_ID, COLUMN_DATE,
			COLUMN_TRANSACTION_TYPE, COLUMN_AMOUNT, COLUMN_CATEGORY,
			COLUMN_REMARKS };

	public TransactionsDatabaseHelper(Context context) {
		dbHelper = MySQLiteHelper.getInstance(context);
	}

	public boolean isDatabaseOpen() {
		if (db == null || db.isOpen() == false)
			return false;
		else
			return true;
	}

	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public long createTransaction(Date date, String type, float amount,
			String remarks, String category) {
		try {
			if (db != null) {
				ContentValues transaction = new ContentValues();
				transaction.put(COLUMN_DATE, date.getTime());
				transaction.put(COLUMN_TRANSACTION_TYPE, type);
				transaction.put(COLUMN_AMOUNT, amount);
				transaction.put(COLUMN_REMARKS, remarks);
				transaction.put(COLUMN_CATEGORY, category);

				long chk = db.insert(TABLE_TRANSACTIONS, null, transaction);

				if (chk != -1) {
					Log.d(getClass().getSimpleName(),
							"Record added successfully : " + chk);
					return chk;
				} else {
					Log.d(getClass().getSimpleName(),
							"Record added added failed...! ");
				}
			} else {
				// Toast.makeText(myContext,
				// "could Not connected database..!!! ",Toast.LENGTH_LONG).show();
			}
		} catch (SQLiteException exc) {
			// Toast.makeText(myContext,
			// "Record could not saved...!!! ",Toast.LENGTH_LONG).show();
		}
		return 0;
	}

	public List<Transaction> getAllTransactions() {
		Cursor cursor = getAllTransactionsCursor();

		return cursorToList(cursor);
	}

	public Cursor getAllTransactionsCursor() {
		return db.query(TABLE_TRANSACTIONS, allColumns, null, null, null, null,
				null);
	}

	public List<Transaction> getTransactionsBetweenDates(Date startDate,
			Date endDate) {

		Cursor cursor = getTransactionsBetweenDatesCursor(startDate, endDate);

		Log.d(getClass().getSimpleName() + ".getTransactionsBetweenDates()",
				"number of transactions this month: " + cursor.getCount());

		List<Transaction> transactionsList = cursorToList(cursor);

		// close the cursor
		cursor.close();

		return transactionsList;

	}

	private List<Transaction> cursorToList(Cursor cursor) {
		List<Transaction> transactions = new ArrayList<Transaction>();

		while (cursor.moveToNext()) {
			Transaction transaction = cursorToTransaction(cursor);
			transactions.add(transaction);
		}

		return transactions;
	}

	public Cursor getTransactionsBetweenDatesCursor(Date startDate, Date endDate) {

		// TODO

		// convert the start and end dates to datetime values
		long startDateTime = startDate.getTime();

		// for the end date we need all the transactions done on that date
		// hence set the datetime to endDate+1 date's start time minus 1
		long endDateTime = getEndDateTime(endDate);

		String selection = COLUMN_DATE + "> ? and " + COLUMN_DATE + " < ?";
		String[] selectionArgs = { Long.toString(startDateTime),
				Long.toString(endDateTime) };
		String orderBy = COLUMN_DATE + " DESC";

		return db.query(TABLE_TRANSACTIONS, allColumns, selection,
				selectionArgs, null, null, orderBy);
	}

	private long getEndDateTime(Date endDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(endDate);
		// TODO in case the endDate is the last date of the month, increase the
		// month
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 1);
		c.set(Calendar.HOUR, 00);
		c.set(Calendar.MINUTE, 00);
		c.set(Calendar.SECOND, 00);
		return c.getTimeInMillis() - 1;
	}

	public Cursor getFilteredTransactions(ReportFilters filters) {
		// create the where clause string from the filters
		String selection = constructWhereClause(filters);
		String orderBy = COLUMN_DATE + " DESC";
		return db.query(TABLE_TRANSACTIONS, allColumns, selection, null, null,
				null, orderBy);
	}

	private String constructWhereClause(ReportFilters filters) {
		StringBuilder builder = new StringBuilder();

		// dates
		// startdate
		builder.append(COLUMN_DATE + " > "
				+ Long.toString(filters.getStartDate().getTime()));
		builder.append(" and ");
		// endDate
		builder.append(COLUMN_DATE + " < "
				+ Long.toString(getEndDateTime(filters.getEndDate())));

		// remarks
		if (filters.isRemarksContainsSetFlag()) {
			builder.append(" and ");
			builder.append(COLUMN_REMARKS + " like '%"
					+ filters.getRemarksContains() + "%'");
		}

		// categories
		if (filters.getCategories().size() != 0) {
			builder.append(" and ");
			builder.append(COLUMN_CATEGORY + " in " + getCategories(filters));
		}

		// min amount
		if (filters.isMinAmountSetFlag()) {
			builder.append(" and ");
			builder.append(COLUMN_AMOUNT + " >= "
					+ Float.toString(filters.getMinAmount()));
		}

		// max amount
		if (filters.isMaxAmountSetFlag()) {
			builder.append(" and ");
			builder.append(COLUMN_AMOUNT + " <= "
					+ Float.toString(filters.getMaxAmount()));
		}

		Log.d(getClass().getSimpleName() + ".constructWhereClause()",
				"selection : " + builder.toString());

		return builder.toString();
	}

	private String getCategories(ReportFilters filters) {
		List<String> categoryStrings = filters.getCategories();

		StringBuilder csb = new StringBuilder();
		csb.append("(");
		for (String categoryString : categoryStrings) {
			csb.append("'");
			csb.append(categoryString);
			csb.append("',");
		}
		csb.setCharAt(csb.lastIndexOf(","), ' ');
		csb.append(")");

		Log.d("TransactionsDatabaseHelper.getCategories()",
				"categories string : " + csb.toString());

		return csb.toString();
	}

	private Transaction cursorToTransaction(Cursor cursor) {
		long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
		float amount = cursor.getFloat(cursor.getColumnIndex(COLUMN_AMOUNT));
		long dateTime = cursor.getLong((cursor.getColumnIndex(COLUMN_DATE)));
		Date date = new Date(dateTime);
		String remarks = cursor
				.getString(cursor.getColumnIndex(COLUMN_REMARKS));
		String category = cursor.getString(cursor
				.getColumnIndex(COLUMN_CATEGORY));
		String type = cursor.getString(cursor
				.getColumnIndex(COLUMN_TRANSACTION_TYPE));

		return new Transaction(id, type, amount, date, remarks, category);
	}

	public Transaction getTransaction(long _id) {
		String selection = COLUMN_ID + "= ?";
		String[] selectionArgs = { Long.toString(_id) };
		Cursor cursor = db.query(TABLE_TRANSACTIONS, allColumns, selection,
				selectionArgs, null, null, null);
		if (cursor.moveToFirst()) {
			Transaction transaction = cursorToTransaction(cursor);
			cursor.close();
			return transaction;
		} else
			return null;
	}

	public long updateTransaction(long _id, Date date, String type,
			float amount, String remarks, String category) {

		if (db != null) {
			ContentValues transaction = new ContentValues();
			transaction.put(COLUMN_DATE, date.getTime());
			transaction.put(COLUMN_TRANSACTION_TYPE, type);
			transaction.put(COLUMN_AMOUNT, amount);
			transaction.put(COLUMN_REMARKS, remarks);
			transaction.put(COLUMN_CATEGORY, category);

			String whereClause = COLUMN_ID + " = " + _id;

			int rowsAffected = db.update(TABLE_TRANSACTIONS, transaction,
					whereClause, null);

			if (rowsAffected == 1)
				return _id;
		}

		return -1;
	}

	/**
	 * deletes the transaction with this id
	 * 
	 * returns 0 in case the transaction could not be deleted and 1 in case the
	 * deletion was successful
	 * 
	 * @param _id
	 * @return
	 */
	public int deleteTransaction(long _id) {

		return db.delete(TABLE_TRANSACTIONS, "_id = " + _id, null);
	}
}
