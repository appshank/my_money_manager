package appshank.mymoneymanager;

import java.util.Date;

public class Transaction {

	public static final String KEY_TYPE = "type";

	public static final String TYPE_INCOME = "I";
	public static final String TYPE_EXPENSE = "E";

	private long _id;
	private String type;
	private float amount;
	private Date date;
	private String remarks;
	private String category;

	public Transaction(long id, String type, float amount, Date date,
			String remarks, String category) {
		super();
		this._id = id;
		this.type = type;
		this.amount = amount;
		this.date = date;
		this.remarks = remarks;
		this.category = category;
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		this._id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(type).append(' ').append(amount).append(' ').append(date)
				.append(' ').append(category).append(' ').append(remarks);
		return super.toString();
	}

	// TODO: How to reference to the bill

}
