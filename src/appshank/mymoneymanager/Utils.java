package appshank.mymoneymanager;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class Utils {

	private static boolean currencySymbolSet = false;

	public static int getDPToPixels(Context context, int valueInDP) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				valueInDP, context.getResources().getDisplayMetrics());
	}

	private static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (activity.getCurrentFocus() != null)
			inputMethodManager.hideSoftInputFromWindow(activity
					.getCurrentFocus().getWindowToken(), 0);

	}

	public static void setupUI(View view, final Activity activity) {

		// setup the currencysymbol
		if (!currencySymbolSet) {
			checkAndSetCurrencySymbolAndFormat(activity);
		}

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText || view instanceof RadioButton)) {

			view.setOnTouchListener(new OnTouchListener() {

				public boolean onTouch(View v, MotionEvent event) {
					hideSoftKeyboard(activity);
					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView, activity);
			}
		}
	}

	public static void setupAppliedFiltersUI(View view, final Activity activity) {

		// disable touch on children except the button
		if (!(view.getId() == R.id.button_close || view.getId() == R.id.textView_filters_applied)) {
			view.setEnabled(false);
			view.setClickable(false);
			view.setFocusable(false);
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupAppliedFiltersUI(innerView, activity);
			}
		}
	}

	public static void setVisibility(View view, boolean visible) {
		if (visible)
			view.setVisibility(View.VISIBLE);
		else
			view.setVisibility(View.GONE);
	}

	public static void updateDateTextView(TextView dateView, Calendar c) {
		dateView.setText(c.get(Calendar.DAY_OF_MONTH) + "/"
				+ (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR));
	}

	public static void startToastService(Context context, String toastText) {
		Intent serviceIntent = new Intent(context, MyAppToastService.class);
		Bundle options = new Bundle();
		options.putString("toast_text", toastText);
		serviceIntent.putExtras(options);
		Log.d("Utils.startToastService()", "starting service...");
		context.startService(serviceIntent);
	}

	/**
	 * maps the result code to the respective toast text to be displayed
	 * 
	 * @param resultCode
	 * @return
	 */
	public static String getToastText(int resultCode) {
		String toastText = null;

		switch (resultCode) {
		case Constants.RESULT_CODE_TRANSACTION_DELETED:
			toastText = "Transaction Deleted";
			break;
		case Constants.RESULT_CODE_TRANSACTION_DELETION_FAILED:
			toastText = "Transaction NOT Deleted";
			break;
		case Constants.RESULT_CODE_TRANSACTION_NOT_SAVED:
			toastText = "Transaction NOT Saved !";
			break;
		case Constants.RESULT_CODE_TRANSACTION_SAVED:
			toastText = "Transaction Saved";
			break;
		case Constants.RESULT_CODE_TRANSACTION_UPDATED:
			toastText = "Transaction Updated";
			break;
		case Constants.RESULT_CODE_TRANSACTION_UPDATION_FAILED:
			toastText = "Transaction NOT Updated !";
			break;
		case Constants.RESULT_CODE_FILTERS_SET:
			toastText = "Filters Applied";
			break;
		case Constants.RESULT_CODE_FILTERS_NOT_SET:
			toastText = "Filters NOT Set";
			break;
		case Constants.FILTERS_REMOVED:
			toastText = "Filters Removed";
			break;
		default:
			toastText = "";
			break;
		}

		return toastText;
	}

	public static void checkAndSetCurrencySymbolAndFormat(Context context) {

		// get the shared preferences
		SharedPreferences preferences = context.getSharedPreferences(
				"MyMoneyManager_Preferences", 0);

		// check if the shared preferences exist
		if (preferences.contains("currency_symbol")) {
			// do nothing
			Log.d(Utils.class.getSimpleName() + ".checkAndSetCurrencySymbol()",
					"currency_symbol: "
							+ preferences.getString("currency_symbol", null));
		} else {
			String symbol = getLocaleCurrencySymbol(context);
			String currencyCode = getLocaleCurrencyCode(context);
			Log.d(Utils.class.getSimpleName(), "currency symbol: " + symbol);
			Editor preferencesEditor = preferences.edit();
			preferencesEditor.putString("currency_symbol", symbol);
			preferencesEditor.putString("currency_code", currencyCode);
			preferencesEditor.commit();
		}

		// set the currency symbol as per the preferences
		Settings.CURRENCY_SYMBOL = preferences
				.getString("currency_symbol", "$");
		// set the numberformat for amounts as per the currency
		Currency currency = Currency.getInstance(preferences.getString(
				"currency_code", "USD"));
		Settings.format = numberFormatFromCurrencyCode(currency);

	}

	private static String getLocaleCurrencySymbol(Context context) {
		// in case the preferences dont exist, set the currency according to
		// the locale
		Locale locale = context.getResources().getConfiguration().locale;

		// if not then store the currency symbol in the preferences
		String symbol = Currency.getInstance(locale).getSymbol();
		return symbol;
	}

	private static String getLocaleCurrencyCode(Context context) {
		// in case the preferences dont exist, set the currency according to
		// the locale
		Locale locale = context.getResources().getConfiguration().locale;

		// if not then store the currency symbol in the preferences
		String currencyCode = Currency.getInstance(locale).getCurrencyCode();
		return currencyCode;
	}

	public static String getCurrencySymbol() {
		return Settings.CURRENCY_SYMBOL;
	}

	public static void setCurrencySymbolAndFormat(Context context,
			String currencyCode) {
		// set the currencySymbol as per currencyCode
		Currency currency;
		try {
			currency = Currency.getInstance(currencyCode);
		} catch (IllegalArgumentException e) {
			// do nothing
			Locale locale = context.getResources().getConfiguration().locale;
			currency = Currency.getInstance(locale);
		}

		String currencySymbol = currency.getSymbol();

		// get the shared preferences and update the currency symbol
		SharedPreferences preferences = context.getSharedPreferences(
				"MyMoneyManager_Preferences", 0);
		Editor preferencesEditor = preferences.edit();
		preferencesEditor.putString("currency_symbol", currencySymbol);
		preferencesEditor.putString("currency_code", currencyCode);
		preferencesEditor.commit();

		// update the "Settings"
		// currency symbol
		Settings.CURRENCY_SYMBOL = preferences.getString("currency_symbol",
				getLocaleCurrencySymbol(context));
		// number format for formatting the currency
		Settings.format = numberFormatFromCurrencyCode(currency);
	}

	private static NumberFormat numberFormatFromCurrencyCode(Currency currency) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(currency
				.getDefaultFractionDigits());
		numberFormat.setCurrency(currency);
		return numberFormat;
	}
}
