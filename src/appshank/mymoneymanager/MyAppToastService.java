package appshank.mymoneymanager;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class MyAppToastService extends IntentService {

	Handler handler = new Handler();

	public MyAppToastService() {
		super("MyAppToastService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// wait for 500ms before starting the toast
		Log.d("WidgetToastService", "inside onHandleIntent()");

		// display the toast
		String toastText = "";
		if (intent.hasExtra("toast_text")) {
			toastText = intent.getStringExtra("toast_text");
		}

		handler.post(new MyRunnable(toastText));
	}

	public class MyRunnable implements Runnable {
		String toastText;

		public MyRunnable(String toastText) {
			this.toastText = toastText;
		}

		@Override
		public void run() {
			// Log.d("WidgetToastService", "about to show toast");
			Toast.makeText(getApplicationContext(), toastText,
					Toast.LENGTH_SHORT).show();

		}

	}

}
