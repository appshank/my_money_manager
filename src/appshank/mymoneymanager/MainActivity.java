package appshank.mymoneymanager;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class MainActivity extends Activity {

	private TransactionsDatabaseHelper transactionsDataSource = null;

	private float expensesThisMonth;
	private float incomesThisMonth;

	public static final String TOAST_TRANSACTION_SAVED = "Transaction Saved";
	public static final String TOAST_TRNASACTION_NOT_SAVED = "Transaction NOT Saved !";

	TextView textViewExpenses;
	TextView textViewIncomes;
	TextView textViewCurrencySymbolExpenses;
	TextView textViewCurrencySymbolIncomes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		if (transactionsDataSource == null)
			transactionsDataSource = new TransactionsDatabaseHelper(this);

		transactionsDataSource.open();

		// get the currency symbol
		// initialize the currency symbol
		Utils.checkAndSetCurrencySymbolAndFormat(this);

		textViewCurrencySymbolExpenses = (TextView) findViewById(R.id.textView_currency_symbol_expenses);
		textViewCurrencySymbolExpenses.setText(Utils.getCurrencySymbol() + " ");
		textViewCurrencySymbolIncomes = (TextView) findViewById(R.id.textView_currency_symbol_incomes);
		textViewCurrencySymbolIncomes.setText(Utils.getCurrencySymbol() + " ");

		// transactionsDataSource.upgradeTable(); // TEMP

		// TODO: calculate the total and update the expensesThisMonth variable
		// retrieve the values between startDate and todayDate from the database

		// display the expenses on the screen
		updateExpenses();

	}

	@Override
	protected void onStart() {
		super.onStart();

		EasyTracker.getInstance().activityStart(this);
	}

	public void onClickAddTransaction(View view) {
		// create intent and start activity
		Intent intent = new Intent(MainActivity.this, TransactionActivity.class);
		//
		startActivityForResult(intent, Constants.REQUEST_CODE_ADD_TRANSACTION);
	}

	public void onClickViewReport(View view) {
		// TODO

		// create the intent for Viewing the Transactions Report
		Intent intent = new Intent(MainActivity.this, ReportActivity.class);
		startActivity(intent);
	}

	public void onClickSettings(View view) {
		// TODO
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();

		if (requestCode == Constants.REQUEST_CODE_ADD_TRANSACTION) {
			switch (resultCode) {
			case Constants.RESULT_CODE_TRANSACTION_SAVED:
				// update the transaction total
				updateExpenses();
				break;
			case Constants.RESULT_CODE_TRANSACTION_NOT_SAVED:
				// do nothing
				break;
			case RESULT_CANCELED:
				// display the toast the the transaction was not saved
				Toast.makeText(MainActivity.this, TOAST_TRNASACTION_NOT_SAVED,
						Toast.LENGTH_SHORT).show();
				break;
			}

			return;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void updateExpenses() {
		// get total from the transactions database for the current month
		updateTotalsThisMonth();

		if (textViewExpenses == null)
			textViewExpenses = (TextView) findViewById(R.id.textView_expenses_so_far);
		textViewExpenses.setText(Settings.format.format(expensesThisMonth));
		if (textViewIncomes == null)
			textViewIncomes = (TextView) findViewById(R.id.textView_incomes_so_far);
		textViewIncomes.setText(Settings.format.format(incomesThisMonth));
	}

	private void updateTotalsThisMonth() {
		Date monthStartDate = getMonthStartDate();

		// TEMP
		List<Transaction> transactions = transactionsDataSource
				.getTransactionsBetweenDates(monthStartDate, new Date());

		float incomesTotal = 0f;
		float expenseTotal = 0f;

		int numOfTransactions = transactions.size();
		// Log.d(getClass().getSimpleName(), "number of transactions : "
		// + numOfTransactions);
		Transaction transaction;
		for (int i = 0; i < numOfTransactions; i++) {
			// Log.d("Shank", transactions.get(i).toString());
			transaction = transactions.get(i);
			if (transaction.getType().equalsIgnoreCase("E"))
				expenseTotal += transactions.get(i).getAmount();
			else if (transaction.getType().equalsIgnoreCase("I"))
				incomesTotal += transactions.get(i).getAmount();
		}

		expensesThisMonth = expenseTotal;
		incomesThisMonth = incomesTotal;

	}

	private Date getMonthStartDate() {

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR, 00);
		cal.set(Calendar.MINUTE, 00);
		cal.set(Calendar.SECOND, 00);

		return cal.getTime();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// update the currencySymbol
		textViewCurrencySymbolExpenses.setText(Utils.getCurrencySymbol() + " ");
		textViewCurrencySymbolIncomes.setText(Utils.getCurrencySymbol() + " ");

		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();

		updateExpenses();
	}

	@Override
	protected void onStop() {
		super.onStop();

		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	protected void onDestroy() {
		Log.d(getClass().getSimpleName(), "inside onDestroy()");

		// close the datasource
		if (transactionsDataSource != null) {
			transactionsDataSource.close();
		}

		super.onDestroy();
	}

}
