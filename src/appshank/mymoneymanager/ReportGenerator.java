package appshank.mymoneymanager;


public interface ReportGenerator {

	public String generateReport();

}
