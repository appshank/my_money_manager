package appshank.mymoneymanager;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TransactionCursorAdapter extends CursorAdapter {

	// reference to calendar variable
	Calendar c;

	// flag for landscape mode
	boolean isLandscape;

	@SuppressWarnings("deprecation")
	public TransactionCursorAdapter(Context context, Cursor cursor,
			boolean isLandscape) {
		super(context, cursor);
		this.isLandscape = isLandscape;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// the cursor automatically moves to each item hence we do not need to
		// do any cursor movement stuff

		final long _id = cursor.getLong(cursor
				.getColumnIndex(TransactionsDatabaseHelper.COLUMN_ID));

		// s.no.
		TextView s_no = (TextView) view.findViewById(R.id.tListItem_s_no);
		s_no.setText(Integer.toString(cursor.getPosition() + 1));

		// date
		TextView date = (TextView) view.findViewById(R.id.tListItem_date);
		long dateTime = cursor.getLong((cursor
				.getColumnIndex(TransactionsDatabaseHelper.COLUMN_DATE)));
		if (c == null)
			c = Calendar.getInstance();
		c.setTimeInMillis(dateTime);
		StringBuilder dateBuilder = new StringBuilder();
		dateBuilder.append(c.get(Calendar.DAY_OF_MONTH));
		dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.MONTH) + 1);
		dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.YEAR));
		date.setText(dateBuilder);

		// type
		String type = cursor
				.getString(cursor
						.getColumnIndex(TransactionsDatabaseHelper.COLUMN_TRANSACTION_TYPE));

		// category
		ImageView categoryImageView = (ImageView) view
				.findViewById(R.id.tListItem_category);
		String categoryString = cursor.getString(cursor
				.getColumnIndex(TransactionsDatabaseHelper.COLUMN_CATEGORY));
		Enum category = null;
		if (type.equalsIgnoreCase("I")) {
			// get the category
			category = CategoryUtils
					.getIncomeCategoryFromString(categoryString);
		} else if (type.equalsIgnoreCase("E")) {
			// get the category
			category = CategoryUtils
					.getExpenseCategoryFromString(categoryString);
		}
		// get the drawable for category
		int drawableId = CategoryUtils.getCategoryResetDrawable(category);
		// set the drawable for this image view
		categoryImageView.setImageResource(drawableId);

		// amount
		TextView amount = (TextView) view.findViewById(R.id.tListItem_amount);
		float floatAmount = cursor.getFloat(cursor
				.getColumnIndex(TransactionsDatabaseHelper.COLUMN_AMOUNT));
		amount.setText(Settings.format.format(floatAmount));
		Log.d(getClass().getSimpleName() + ".bindView()", "type: " + type);
		if (type.equalsIgnoreCase("I"))
			amount.setTextAppearance(context, R.style.amount_income);
		else if (type.equalsIgnoreCase("E"))
			amount.setTextAppearance(context, R.style.amount_expense);

		/*
		 * // remarks (in case of landscape) if (isLandscape) { TextView remarks
		 * = (TextView) view .findViewById(R.id.tListItem_remarks); String
		 * remarksString = cursor.getString(cursor
		 * .getColumnIndex(TransactionsDatabaseHelper.COLUMN_REMARKS));
		 * Log.d(getClass().getSimpleName(), "remarksString: " + remarksString);
		 * if (remarksString == null) remarksString = "";
		 * remarks.setText(remarksString); }
		 */

		// view details
		TextView button_view = (TextView) view.findViewById(R.id.view_edit);
		button_view.setOnClickListener(new TransactionButtonOnClickListener(
				_id, context));
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {

		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.transaction_list_item, parent,
				false);

		return view;
	}

	private class TransactionButtonOnClickListener implements OnClickListener {

		long _id;
		Context context;

		public TransactionButtonOnClickListener(long id, Context context) {
			_id = id;
			this.context = context;
		}

		@Override
		public void onClick(View v) {
			Log.d("TransactionCursorAdapter.onBind()", "button clicked");
			Log.d("TransactionCursorAdapter.onBind()", "id: " + _id);

			// create the intent for TransactionActivity
			Intent intent = new Intent(context, TransactionActivity.class);

			// add the extras to the intent
			Bundle options = new Bundle();
			options.putLong("_id", _id);
			options.putInt(TransactionActivity.KEY_MODE,
					TransactionActivity.MODE_VIEW);
			intent.putExtras(options);

			// start the activity for result
			Log.d("TransactionCursorAdapter.oTransactionButtonOnClickListener",
					"starting transaction activity");
			((Activity) context).startActivityForResult(intent,
					Constants.REQUEST_CODE_VIEW_TRANSACTION);
		}
	}

}
