package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import appshank.mymoneymanager.Category.ExpenseCategory;
import appshank.mymoneymanager.Category.IncomeCategory;

import com.google.analytics.tracking.android.EasyTracker;

public class TransactionActivity extends Activity {

	public static final String TOAST_TRANSACTION_DELETED = "Transaction Deleted...";
	public static final String TOAST_TRANSACTION_NOT_DELETED = "Transaction NOT Deleted !";

	private TransactionsDatabaseHelper transactionsDataSource;

	// the different modes of using this activity
	public static final String KEY_MODE = "mode";
	public static final int MODE_ADD = 0;
	public static final int MODE_EDIT = 1;
	public static final int MODE_VIEW = 2;

	protected static final int DATE_DIALOG_ID = 0;

	// current mode
	int mode;

	// flag to check if the activity was started from a widget
	boolean fromWidget = false;

	// details about the transaction
	Transaction transaction;
	long _id;
	String type;
	float amount;
	Date date;
	String remarks;
	String category;

	// details about the date
	final Calendar c = Calendar.getInstance();
	private int year;
	private int month;
	private int day;

	// references to the fields regarding the transaction
	RadioGroup radioGroup;
	RadioButton radioButtonIncome;
	RadioButton radioButtonExpense;
	EditText editTextAmount;
	EditText editTextDate;
	EditText editTextRemarks;
	ImageView imageViewSelectedCategory;
	TextView textViewSelectedCategory;
	Button buttonSave;
	Button buttonDelete;

	View expensesCategoryLayout;
	View incomesCategoryLayout;

	private List<ImageView> categoryImageViews;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_transaction);

		if (transactionsDataSource == null)
			transactionsDataSource = new TransactionsDatabaseHelper(this);

		transactionsDataSource.open();

		// initialize the references to the various view components
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		radioButtonIncome = (RadioButton) findViewById(R.id.radioButton_income);
		radioButtonExpense = (RadioButton) findViewById(R.id.radioButton_expense);
		editTextAmount = (EditText) findViewById(R.id.editText_amount);
		editTextDate = (EditText) findViewById(R.id.editText_date);
		editTextRemarks = (EditText) findViewById(R.id.editText_remark);
		buttonSave = (Button) findViewById(R.id.button_viewReport);
		buttonDelete = (Button) findViewById(R.id.button_delete_transaction);
		textViewSelectedCategory = (TextView) findViewById(R.id.textView_selectedCategory);

		expensesCategoryLayout = findViewById(R.id.layout_expenses_categories);
		incomesCategoryLayout = findViewById(R.id.layout_incomes_categories);

		// setup the UI to remove the soft keyboard when the user click outside
		// of edit text etc
		Utils.setupUI(findViewById(R.id.layout_add_transaction), this);

		// retrieve the extras that were passed along with this intent
		Bundle options = getIntent().getExtras();
		if (options != null) {
			Log.d("TransactionActivity.onCreate()", "options keyset: "
					+ options.keySet());

			if (options.containsKey(KEY_MODE)) {
				mode = options.getInt(KEY_MODE);
			} else {
				mode = MODE_ADD;
			}

			if (options.containsKey(Transaction.KEY_TYPE)) {
				type = options.getString(Transaction.KEY_TYPE);
				Log.d("TransactionActivity.onCreate()", "type = " + type);
			}

			Log.d("TransactionActivity.onCreate()",
					"option contain key from_widget: "
							+ options.containsKey("from_widget"));

			if (options.containsKey("from_widget")) {
				Log.d("TransactionActivity.onCreate()", "fromWidget: "
						+ options.getBoolean("from_widget"));
				fromWidget = options.getBoolean("from_widget");
			}
		}

		// set the values as per the mode of the activity
		initCategoryImageViews();
		switch (mode) {
		case MODE_EDIT:
		case MODE_VIEW:
			_id = getIntent().getExtras().getLong("_id");
			Log.d("TransactionActivity.onCreate", "_id: " + _id);
			transaction = transactionsDataSource.getTransaction(_id);
			populateValuesAsPerTransaction();

			// initialize the calendar (year, month and day)
			initializeCalendar(date);

			// make the fields in-editable
			makeFieldsInEditable();

			// the save button becomes the edit button and shall enable
			// editing of the fields when clicked
			buttonSave.setText("EDIT");
			break;
		case MODE_ADD:
			makeFieldsEditable();

			// make the delete button gone
			buttonDelete.setVisibility(View.GONE);
		default:
			initializeDefaultValues(type);
			break;
		}

		populateViewsByValues();

		setListenersOnViews();

	}

	private void initCategoryImageViews() {
		categoryImageViews = new ArrayList<ImageView>();

		// store references for all the image views in the arraylist
		ImageView categoryImageView;

		// expenses
		categoryImageView = (ImageView) findViewById(R.id.cat_bill);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_books_n_stationery);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_party_n_drinks);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_cinema_parties_n_outings);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_cosmetics);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_food);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_fuel_n_conveyance);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_groceries);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_kids);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_shopping);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_rent_n_mortage);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_default_expense);
		categoryImageViews.add(categoryImageView);

		// incomes
		categoryImageView = (ImageView) findViewById(R.id.cat_salary);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_payment_from_others);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_interest_n_investments);
		categoryImageViews.add(categoryImageView);
		categoryImageView = (ImageView) findViewById(R.id.cat_default_income);
		categoryImageViews.add(categoryImageView);

	}

	@Override
	protected void onStart() {
		super.onStart();

		EasyTracker.getInstance().activityStart(this);
	}

	private void initializeCalendar(Date date) {
		c.setTime(date);
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
	}

	private void makeFieldsInEditable() {
		changeFieldsEditability(false);
	}

	private void changeFieldsEditability(boolean isEditable) {
		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for radioGroup");
		changeEditability(radioGroup, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for radioButtonIncome");
		changeEditability(radioButtonIncome, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for radioButtonExpense");
		changeEditability(radioButtonExpense, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for editTextAmount");
		changeEditability(editTextAmount, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for editTextDate");
		changeEditability(editTextDate, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for editTextRemarks");
		changeEditability(editTextRemarks, isEditable);

		Log.d("TransactionActivity.changeFieldsEditability()",
				"changing editability for categoryImageViews");
		for (ImageView imageView : categoryImageViews) {
			Log.d("TransactionActivity.changeFieldsEditability()",
					"changing editability for imageView");
			changeEditability(imageView, isEditable);
		}

		Log.d("TransactionActivity.changeFieldsEditability",
				"textViewDate.isClickable(): " + editTextDate.isClickable());
	}

	private void changeEditability(View view, boolean isEditable) {
		view.setEnabled(isEditable);
		view.setClickable(isEditable);

		if (view == editTextAmount) {
			if (isEditable) {
				((EditText) view).setInputType(InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);
				((EditText) view).setKeyListener(DigitsKeyListener.getInstance(
						false, true));
			} else {
				((EditText) view).setInputType(InputType.TYPE_NULL);
			}
		}

		/*
		 * if (view.getClass() != RadioGroup.class && view.getClass() !=
		 * RadioButton.class) { view.setFocusable(isEditable);
		 * view.setFocusableInTouchMode(isEditable); }
		 */

	}

	private void populateValuesAsPerTransaction() {
		_id = transaction.getId();
		type = transaction.getType();
		amount = transaction.getAmount();
		remarks = transaction.getRemarks();
		date = transaction.getDate();
		category = transaction.getCategory();

		Class categoryClass = type.equalsIgnoreCase(Transaction.TYPE_INCOME) ? IncomeCategory.class
				: ExpenseCategory.class;

		Log.d("populateValuesAsPerTransaction()", "categoryClass : "
				+ categoryClass.getSimpleName());

		setSelectedCategoryImageView(CategoryUtils.getCategoryFromString(
				categoryClass, category));

		// update text view
		setSelectedCategoryTextView(CategoryUtils.getCategoryFromString(
				categoryClass, category));
	}

	private void populateViewsByValues() {
		// type
		if (type == null)
			type = Transaction.TYPE_EXPENSE;

		if (type.equalsIgnoreCase(Transaction.TYPE_INCOME)) {
			radioButtonIncome.setChecked(true);
			setExpensesCategoryLayoutVisibility(false);
		} else if (type.equalsIgnoreCase(Transaction.TYPE_EXPENSE)) {
			radioButtonExpense.setChecked(true);
			setIncomesCategoryLayoutVisibility(false);
		}

		// amount
		if (amount != 0F)
			editTextAmount.setText(Float.toString(amount));

		// date
		updateTextViewDate();

		// remarks
		editTextRemarks.setText(remarks);
	}

	private void setIncomesCategoryLayoutVisibility(boolean visibility) {
		if (visibility)
			incomesCategoryLayout.setVisibility(View.VISIBLE);
		else
			incomesCategoryLayout.setVisibility(View.GONE);
	}

	private void setExpensesCategoryLayoutVisibility(boolean visibility) {
		if (visibility)
			expensesCategoryLayout.setVisibility(View.VISIBLE);
		else
			expensesCategoryLayout.setVisibility(View.GONE);
	}

	private void setListenersOnViews() {

		// radioGroup - type
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				Log.d("onCheckedChanged()", "checkedId: " + checkedId);

				if (checkedId == R.id.radioButton_income) {
					// income
					type = Transaction.TYPE_INCOME;

					// hide the expenses category layout
					setExpensesCategoryLayoutVisibility(false);

					// show the incomes category layout
					setIncomesCategoryLayoutVisibility(true);

					// set the category to be default_income
					setSelectedCategory(IncomeCategory.MISCELLANEOUS_INCOME);

				} else if (checkedId == R.id.radioButton_expense) {
					// expense
					type = Transaction.TYPE_EXPENSE;

					// hide the incomes category layout
					setIncomesCategoryLayoutVisibility(false);

					// show the expenses category layout
					setExpensesCategoryLayoutVisibility(true);

					// set the category to be default_expense
					setSelectedCategory(ExpenseCategory.MISCELLANEOUS_EXPENSE);
				}

				// Log.d("Shank", "type:" + type);
			}

		});

		// amount
		/*
		 * editTextAmount.addTextChangedListener(new TextWatcher() {
		 * 
		 * @Override public void onTextChanged(CharSequence s, int start, int
		 * before, int count) { // Log.d("Shank", "onTextChanged"); }
		 * 
		 * @Override public void beforeTextChanged(CharSequence s, int start,
		 * int count, int after) { // Log.d("Shank", "beforTextChanged:" + s +
		 * " " + start + " " // + count + " " + after); }
		 * 
		 * @Override public void afterTextChanged(Editable s) { //
		 * Log.d("Shank", "afterTextChanged s:" + s); try { amount =
		 * Float.parseFloat(s.toString()); // Log.d("Shank", "new amount:" +
		 * amount); } catch (Exception e) { e.printStackTrace(); } } });
		 */

		// date
		// onClickDate()

		// remarks
		editTextRemarks.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				remarks = s.toString();
			}
		});

	}

	public void onClickSave(View view) {

		// get the intent that had called this activity
		Intent intent = getIntent();

		// save/update the transaction as per mode
		if (mode == MODE_ADD || mode == MODE_EDIT) {

			// update amount as per the field
			String amountString = editTextAmount.getText().toString();
			if (amountString == null || amountString == ""
					|| amountString.length() == 0)
				amountString = "0";
			Log.d("TransactionActivity.onClickSave()", "amountString"
					+ amountString);
			amount = Float.parseFloat(amountString);

			// update category
			category = getSelectedCategory();
			Log.d("TransactionActivity.onClickSave()", "category : " + category);

			// update the date of the transaction
			long id = -1;
			date.setTime(c.getTimeInMillis());
			if (mode == MODE_ADD) {
				id = transactionsDataSource.createTransaction(date, type,
						amount, remarks, category);
			} else if (mode == MODE_EDIT) {
				id = transactionsDataSource.updateTransaction(_id, date, type,
						amount, remarks, category);
			}

			// set the result and
			int resultCode = -1;
			if (mode == MODE_ADD) {
				if (id == -1)
					resultCode = Constants.RESULT_CODE_TRANSACTION_NOT_SAVED;
				else
					resultCode = Constants.RESULT_CODE_TRANSACTION_SAVED;
			} else if (mode == MODE_EDIT) {
				if (id == -1)
					resultCode = Constants.RESULT_CODE_TRANSACTION_UPDATION_FAILED;
				else
					resultCode = Constants.RESULT_CODE_TRANSACTION_UPDATED;
			} else if (id == -1) {
				resultCode = RESULT_CANCELED;

			}
			setResult(resultCode, intent);

			Log.d("TransactionActivity.onClickSave()", "fromWidget: "
					+ fromWidget);

			// start the service WidgetToastService
			String toastText = Utils.getToastText(resultCode);
			Utils.startToastService(this, toastText);

			// close the datasource in case the activity was started from widget
			if (fromWidget)
				transactionsDataSource.close();

			// return to the previous activity
			finish();
		} else if (mode == MODE_VIEW) {
			// make the fields editable
			makeFieldsEditable();

			// change the text on the button to save
			buttonSave.setText("SAVE");

			// change the mode to MODE_EDIT
			mode = MODE_EDIT;

		}
	}

	private String getSelectedCategory() {
		// find the category that this imageView represents
		String categoryString = CategoryUtils.getCategoryString(CategoryUtils
				.getCategoryFromImageView(imageViewSelectedCategory));

		return categoryString;
	}

	private void makeFieldsEditable() {
		changeFieldsEditability(true);
	}

	@SuppressWarnings("deprecation")
	public void onClickDate(View view) {
		Log.d("TransactionActivity.onClickDate()", "view: " + view);
		Log.d("TransactionActivity.onClickDate()", "view.isClickable(): "
				+ view.isClickable());
		Log.d("TransactionActivity.onClickDate()", "date.isClickable(): "
				+ editTextDate.isClickable());
		if (editTextDate.isClickable())
			showDialog(DATE_DIALOG_ID);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DATE_DIALOG_ID:
			((DatePickerDialog) dialog).updateDate(year, month, day);
			break;
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int newYear, int newMonthOfYear,
				int newDayOfMonth) {
			// update the calendar and the year, month, day fields
			if (year != newYear) {
				year = newYear;
				c.set(Calendar.YEAR, year);
			}
			if (month != newMonthOfYear) {
				month = newMonthOfYear;
				c.set(Calendar.MONTH, month);
			}
			if (day != newDayOfMonth) {
				day = newDayOfMonth;
				c.set(Calendar.DAY_OF_MONTH, day);
			}

			// update the text view for the date
			Utils.updateDateTextView(editTextDate, c);
		}

	};

	private void updateTextViewDate() {
		Utils.updateDateTextView(editTextDate, c);
	}

	public void onClickCategory(View view) {
		Log.d("TransactionActivity.onClickCategory()", "view: " + view);

		// find the id of the selected category image view
		int selectedCategory = view.getId();

		// if this category was already selected, do not do anything
		if (imageViewSelectedCategory != null
				&& selectedCategory == imageViewSelectedCategory.getId()) {
			return;
		} else {
			setSelectedCategory(CategoryUtils
					.getCategoryFromImageView((ImageView) view));
		}

	}

	private <T extends Enum> CharSequence getCategoryStringForTextView(
			T category) {
		String selectedCategoryStringForTextView = category.name();

		// convert to lower case
		// replace "_" by " "
		// replace " n " by " and "
		selectedCategoryStringForTextView = selectedCategoryStringForTextView
				.replace("_", " ").replace(" n ", " and ");

		// set the first character of every word to uppercase
		// selectedCategoryStringForTextView.

		return selectedCategoryStringForTextView;
	}

	private void initializeDefaultValues(String type) {

		amount = 0f;
		date = new Date();
		remarks = "";

		if (type == null)
			type = Transaction.TYPE_EXPENSE;

		if (type.equalsIgnoreCase(Transaction.TYPE_EXPENSE)) {
			// set selected category to default expense
			setSelectedCategory(ExpenseCategory.MISCELLANEOUS_EXPENSE);
		} else {
			// set selected category to default expense
			setSelectedCategory(IncomeCategory.MISCELLANEOUS_INCOME);
		}

		// set the year, month and day
		initializeCalendar(date);
	}

	private <T extends Enum> void setSelectedCategory(T category) {
		// update category string
		this.category = category.name();

		// update image view
		setSelectedCategoryImageView(category);

		// update text view
		setSelectedCategoryTextView(category);
	}

	private <T extends Enum> void setSelectedCategoryTextView(T category) {
		// set the textview
		textViewSelectedCategory
				.setText(getCategoryStringForTextView(category));
	}

	private <T extends Enum> void setSelectedCategoryImageView(T category) {
		// reset all the category drawables
		resetAllCategoryImageViews();

		// get the imageview of the argument category
		imageViewSelectedCategory = CategoryUtils.getImageViewForCategory(this,
				category);

		// set its drawable
		imageViewSelectedCategory.setImageResource(CategoryUtils
				.getCategorySetDrawable(category));

	}

	private <T extends Enum> void resetAllCategoryImageViews() {
		for (ImageView imageView : categoryImageViews) {
			T category = CategoryUtils.getCategoryFromImageView(imageView);
			imageView.setImageResource(CategoryUtils
					.getCategoryResetDrawable(category));
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			Log.d("TransactionActivity.onCreateDialog()", "year: " + year);
			Log.d("TransactionActivity.onCreateDialog()", "month: " + month);
			Log.d("TransactionActivity.onCreateDialog()", "day: " + day);
			return new DatePickerDialog(this, mDateSetListener, year, month,
					day);
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_transaction, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickDetele(View view) {
		// get the confirmation from the user for deletion via a alert dialog
		getConfirmationAndDelete();
	}

	private void getConfirmationAndDelete() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage("Sure about deleting this ??");
		builder.setPositiveButton("YES", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				deleteTransaction();
			}

		});
		builder.setNegativeButton("NO", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog confirmationAlertDialog = builder.create();
		confirmationAlertDialog.show();
	}

	private void deleteTransaction() {
		// delete the transaction from the database and return to the previous
		// screen
		int numOfDeletions = transactionsDataSource.deleteTransaction(_id);

		// get the intent that had called this activity
		Intent intent = getIntent();

		// start service to display toast
		int resultCode = (numOfDeletions == 1) ? Constants.RESULT_CODE_TRANSACTION_DELETED
				: Constants.RESULT_CODE_TRANSACTION_DELETION_FAILED;
		String toastText = Utils.getToastText(resultCode);
		Utils.startToastService(this, toastText);

		// set the result code
		setResult(resultCode, intent);

		// return to the previous activity
		finish();
	}

	@Override
	protected void onResume() {

		super.onResume();

		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();
	}

	@Override
	protected void onStop() {
		super.onStop();

		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	protected void onDestroy() {
		Log.d(getClass().getSimpleName(), "inside onDestroy()");

		// close the datasource
		if (transactionsDataSource != null) {
			transactionsDataSource.close();
		}

		super.onDestroy();
	}
}
