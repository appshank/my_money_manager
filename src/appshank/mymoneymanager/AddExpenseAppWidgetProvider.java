package appshank.mymoneymanager;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

public class AddExpenseAppWidgetProvider extends AppWidgetProvider {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("AddExpenseAppWidgetProvider.onReceive()", "inside onReceive() "
				+ intent.getDataString());
		super.onReceive(context, intent);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		final int N = appWidgetIds.length;
		Log.d("AddExpenseAppWidgetProvider.onUpdate()", "N = " + N);

		// Perform this loop procedure for each App Widget that belongs to this
		// provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];

			PendingIntent addExpensePI = getAddExpensePendingIntent(context);
			PendingIntent addIncomePI = getAddIncomePendingIntent(context);

			// get the reference to the App Widget
			RemoteViews views = new RemoteViews(context.getPackageName(),
					R.layout.add_expense_appwidget);

			// attach the click listener for the service start command intent
			views.setOnClickPendingIntent(R.id.widget_add_income_button,
					addIncomePI);
			views.setOnClickPendingIntent(R.id.widget_add_expense_button,
					addExpensePI);

			// Tell the AppWidgetManager to perform an update on the current app
			// widget
			appWidgetManager.updateAppWidget(appWidgetId, views);

		}
	}

	private PendingIntent getAddIncomePendingIntent(Context context) {
		// Create the intent to launch the add transaction activity
		Intent intent = new Intent(context, TransactionActivity.class);
		// create the information bundle to provide information to the
		// activity
		Bundle options = new Bundle();
		options.putString(Transaction.KEY_TYPE, Transaction.TYPE_INCOME);
		options.putInt(TransactionActivity.KEY_MODE,
				TransactionActivity.MODE_ADD);
		options.putBoolean("from_widget", true);
		intent.putExtras(options);

		Log.d("AppWidgetProvider.onUpdate()", "options keyset: "
				+ intent.getExtras().keySet());

		// create the pending intent for the activity
		PendingIntent myPI = PendingIntent.getActivity(context,
				(int) (Math.random() * 10000), intent, 0);

		return myPI;
	}

	private PendingIntent getAddExpensePendingIntent(Context context) {
		// Create the intent to launch the add transaction activity
		Intent intent = new Intent(context, TransactionActivity.class);
		// create the information bundle to provide information to the
		// activity
		Bundle options = new Bundle();
		options.putString(Transaction.KEY_TYPE, Transaction.TYPE_EXPENSE);
		options.putInt(TransactionActivity.KEY_MODE,
				TransactionActivity.MODE_ADD);
		options.putBoolean("from_widget", true);
		intent.putExtras(options);

		Log.d("AppWidgetProvider.onUpdate()", "options keyset: "
				+ intent.getExtras().keySet());

		// create the pending intent for the activity
		PendingIntent myPI = PendingIntent.getActivity(context,
				(int) (Math.random() * 10000), intent, 0);

		return myPI;

	}
}
