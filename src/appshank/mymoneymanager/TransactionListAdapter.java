package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TransactionListAdapter extends BaseAdapter {

	// the element to store all the transactions from the database
	private List<Transaction> transactions = new ArrayList<Transaction>();

	// reference to calendar variable
	Calendar c;

	public TransactionListAdapter() {
		super();
		/*
		 * transactions.add(new Transaction(1L, "I", 43.5F, new Date(),
		 * "remarks 1")); transactions.add(new Transaction(2L, "I", 43.5F, new
		 * Date(), "remarks 2")); transactions.add(new Transaction(3L, "E",
		 * 43.5F, new Date(), "remarks 3")); transactions.add(new
		 * Transaction(4L, "I", 43.5F, new Date(), "remarks 4"));
		 */

		// get all the transactions from the database

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return transactions.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return transactions.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// check if view is null
		if (convertView == null) {
			// get the layout inflater
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			// inflate the view according to the transaction_item_layout
			convertView = inflater.inflate(R.layout.transaction_list_item,
					parent, false);
		}

		// get the transaction and populate the fields according to the
		// transaction
		Transaction transaction = transactions.get(position);

		TextView s_no = (TextView) convertView
				.findViewById(R.id.tListItem_s_no);
		s_no.setText(Long.toString(transaction.getId()));

		TextView date = (TextView) convertView
				.findViewById(R.id.tListItem_date);
		Date tempDate = transaction.getDate();
		if (c == null)
			c = Calendar.getInstance();
		c.setTime(tempDate);
		StringBuilder dateBuilder = new StringBuilder();
		dateBuilder.append(c.get(Calendar.MONTH) + 1);
		dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.DATE));
		dateBuilder.append('/');
		dateBuilder.append(c.get(Calendar.YEAR));
		date.setText(dateBuilder);

		//TextView type = (TextView) convertView.findViewById(R.id.tListItem_i_e);
		//type.setText(transaction.getType());

		TextView amount = (TextView) convertView
				.findViewById(R.id.tListItem_amount);
		amount.setText(Float.toString(transaction.getAmount()));

		return convertView;
	}
}
