package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.widget.ImageView;
import appshank.mymoneymanager.Category.ExpenseCategory;
import appshank.mymoneymanager.Category.IncomeCategory;

public class CategoryUtils {

	public static <T extends Enum> List<String> getCategoryStrings(
			Class<T> categoryClass) {

		Enum[] categoryEnums = null;

		if (categoryClass == ExpenseCategory.class) {
			categoryEnums = ExpenseCategory.values();
		} else if (categoryClass == IncomeCategory.class) {
			categoryEnums = IncomeCategory.values();
		} else {
			throw new IllegalArgumentException(
					"unknown category passed as argument");
		}

		List<String> categoryStrings = new ArrayList<String>();

		for (Enum categoryEnum : categoryEnums) {
			categoryStrings.add(categoryEnum.name());
		}

		return categoryStrings;
	}

	public static <T extends Enum> ImageView getImageViewForCategory(
			Activity activity, T selectedCategory) {
		ImageView imageView = null;

		if (selectedCategory.getClass() == ExpenseCategory.class) {
			switch ((ExpenseCategory) selectedCategory) {
			case PARTY_N_DRINKS:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_party_n_drinks);
				break;
			case BILL:
				imageView = (ImageView) activity.findViewById(R.id.cat_bill);
				break;
			case BOOKS_N_STATIONERY:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_books_n_stationery);
				break;
			case CINEMA_PARTIES_N_OUTINGS:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_cinema_parties_n_outings);
				break;
			case COSMETICS:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_cosmetics);
				break;
			case FOOD:
				imageView = (ImageView) activity.findViewById(R.id.cat_food);
				break;
			case FUEL_N_CONVEYANCE:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_fuel_n_conveyance);
				break;
			case GROCERY:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_groceries);
				break;
			case KIDS:
				imageView = (ImageView) activity.findViewById(R.id.cat_kids);
				break;
			case SHOPPING:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_shopping);
				break;
			case RENT_N_MORTGAGE:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_rent_n_mortage);
				break;
			case MISCELLANEOUS_EXPENSE:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_default_expense);
				break;
			}
		} else if (selectedCategory.getClass() == IncomeCategory.class) {
			switch ((IncomeCategory) selectedCategory) {
			case SALARY:
				imageView = (ImageView) activity.findViewById(R.id.cat_salary);
				break;
			case PAYMENT_FROM_OTHERS:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_payment_from_others);
				break;
			case INTEREST_N_INVESTMENTS:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_interest_n_investments);
				break;
			case MISCELLANEOUS_INCOME:
				imageView = (ImageView) activity
						.findViewById(R.id.cat_default_income);
				break;
			}
		}

		if (imageView == null)
			throw new IllegalStateException("wrong category sought for !");

		return imageView;
	}

	public static <T extends Enum> int getCategorySetDrawable(T category) {
		int selectedDrawableResource = 0;

		if (category.getClass() == ExpenseCategory.class) {
			switch ((ExpenseCategory) category) {
			case PARTY_N_DRINKS:
				selectedDrawableResource = R.drawable.party_n_drinks_selected;
				break;
			case BILL:
				selectedDrawableResource = R.drawable.bill_selected;
				break;
			case BOOKS_N_STATIONERY:
				selectedDrawableResource = R.drawable.books_n_stationery_selected;
				break;
			case CINEMA_PARTIES_N_OUTINGS:
				selectedDrawableResource = R.drawable.cinema_parties_n_outings_selected;
				break;
			case COSMETICS:
				selectedDrawableResource = R.drawable.cosmetics_selected;
				break;
			case FOOD:
				selectedDrawableResource = R.drawable.food_selected;
				break;
			case FUEL_N_CONVEYANCE:
				selectedDrawableResource = R.drawable.fuel_n_conveyance_selected;
				break;
			case GROCERY:
				selectedDrawableResource = R.drawable.grocery_selected;
				break;
			case KIDS:
				selectedDrawableResource = R.drawable.kids_selected;
				break;
			case SHOPPING:
				selectedDrawableResource = R.drawable.shopping_selected;
				break;
			case RENT_N_MORTGAGE:
				selectedDrawableResource = R.drawable.rent_n_mortgage_selected;
				break;
			case MISCELLANEOUS_EXPENSE:
				selectedDrawableResource = R.drawable.default_expense_selected;
				break;
			}
		} else if (category.getClass() == IncomeCategory.class) {
			switch ((IncomeCategory) category) {
			case SALARY:
				selectedDrawableResource = R.drawable.salary_selected;
				break;
			case PAYMENT_FROM_OTHERS:
				selectedDrawableResource = R.drawable.payment_from_others_selected;
				break;
			case INTEREST_N_INVESTMENTS:
				selectedDrawableResource = R.drawable.interest_n_investments_selected;
				break;
			case MISCELLANEOUS_INCOME:
				selectedDrawableResource = R.drawable.default_income_selected;
				break;
			}
		}

		if (selectedDrawableResource == 0)
			throw new IllegalStateException("wrong category sought for !");

		return selectedDrawableResource;
	}

	public static <T extends Enum> int getCategoryResetDrawable(T category) {
		int selectedDrawableResource = 0;
		if (category.getClass() == ExpenseCategory.class) {
			switch ((ExpenseCategory) category) {
			case PARTY_N_DRINKS:
				selectedDrawableResource = R.drawable.party_n_drinks;
				break;
			case BILL:
				selectedDrawableResource = R.drawable.bill;
				break;
			case BOOKS_N_STATIONERY:
				selectedDrawableResource = R.drawable.books_n_stationery;
				break;
			case CINEMA_PARTIES_N_OUTINGS:
				selectedDrawableResource = R.drawable.cinema_parties_n_outings;
				break;
			case COSMETICS:
				selectedDrawableResource = R.drawable.cosmetics;
				break;
			case FOOD:
				selectedDrawableResource = R.drawable.food;
				break;
			case FUEL_N_CONVEYANCE:
				selectedDrawableResource = R.drawable.fuel_n_conveyance;
				break;
			case GROCERY:
				selectedDrawableResource = R.drawable.grocery;
				break;
			case KIDS:
				selectedDrawableResource = R.drawable.kids;
				break;
			case SHOPPING:
				selectedDrawableResource = R.drawable.shopping;
				break;
			case RENT_N_MORTGAGE:
				selectedDrawableResource = R.drawable.rent_n_mortgage;
				break;
			case MISCELLANEOUS_EXPENSE:
				selectedDrawableResource = R.drawable.default_expense;
				break;
			}
		} else if (category.getClass() == IncomeCategory.class) {
			switch ((IncomeCategory) category) {
			case SALARY:
				selectedDrawableResource = R.drawable.salary;
				break;
			case PAYMENT_FROM_OTHERS:
				selectedDrawableResource = R.drawable.payment_from_others;
				break;
			case INTEREST_N_INVESTMENTS:
				selectedDrawableResource = R.drawable.interest_n_investments;
				break;
			case MISCELLANEOUS_INCOME:
				selectedDrawableResource = R.drawable.default_income;
				break;
			}
		}

		if (selectedDrawableResource == 0)
			throw new IllegalStateException("wrong category sought for !");

		return selectedDrawableResource;
	}

	public static <T extends Enum> T getCategoryFromImageView(
			ImageView imageView) {
		int id = imageView.getId();

		T category = null;

		switch (id) {
		case R.id.cat_party_n_drinks:
			category = (T) ExpenseCategory.PARTY_N_DRINKS;
			break;
		case R.id.cat_bill:
			category = (T) ExpenseCategory.BILL;
			break;
		case R.id.cat_books_n_stationery:
			category = (T) ExpenseCategory.BOOKS_N_STATIONERY;
			break;
		case R.id.cat_cinema_parties_n_outings:
			category = (T) ExpenseCategory.CINEMA_PARTIES_N_OUTINGS;
			break;
		case R.id.cat_cosmetics:
			category = (T) ExpenseCategory.COSMETICS;
			break;
		case R.id.cat_food:
			category = (T) ExpenseCategory.FOOD;
			break;
		case R.id.cat_fuel_n_conveyance:
			category = (T) ExpenseCategory.FUEL_N_CONVEYANCE;
			break;
		case R.id.cat_groceries:
			category = (T) ExpenseCategory.GROCERY;
			break;
		case R.id.cat_kids:
			category = (T) ExpenseCategory.KIDS;
			break;
		case R.id.cat_shopping:
			category = (T) ExpenseCategory.SHOPPING;
			break;
		case R.id.cat_rent_n_mortage:
			category = (T) ExpenseCategory.RENT_N_MORTGAGE;
			break;
		case R.id.cat_default_expense:
			category = (T) ExpenseCategory.MISCELLANEOUS_EXPENSE;
			break;
		case R.id.cat_salary:
			category = (T) IncomeCategory.SALARY;
			break;
		case R.id.cat_payment_from_others:
			category = (T) IncomeCategory.PAYMENT_FROM_OTHERS;
			break;
		case R.id.cat_interest_n_investments:
			category = (T) IncomeCategory.INTEREST_N_INVESTMENTS;
			break;
		case R.id.cat_default_income:
			category = (T) IncomeCategory.MISCELLANEOUS_INCOME;
			break;
		}

		Log.d("getCategoryFromImageView()", "category as per imageView : "
				+ category.name());

		return category;
	}

	public static <T extends Enum> String getCategoryString(T category) {
		String categoryString = category.name();

		return categoryString;
	}

	public static ExpenseCategory getExpenseCategoryFromString(
			String categoryString) {
		return getCategoryFromString(ExpenseCategory.class, categoryString);
	}

	public static IncomeCategory getIncomeCategoryFromString(
			String categoryString) {
		return getCategoryFromString(IncomeCategory.class, categoryString);
	}

	public static <T extends Enum> T getCategoryFromString(
			Class<T> categoryClass, String categoryString) {
		Log.d("getCategoryFromString()", "categoryString : " + categoryString);

		T[] categoryArray = null;

		if (categoryClass == ExpenseCategory.class) {
			categoryArray = (T[]) ExpenseCategory.values();
		} else if (categoryClass == IncomeCategory.class) {
			categoryArray = (T[]) IncomeCategory.values();
		} else {
			throw new IllegalArgumentException("unknown category type!");
		}

		for (T category : categoryArray) {

			if (category.name().equalsIgnoreCase(categoryString))
				return category;
		}

		return null;
	}

	public static Enum getCategoryFromString(String categoryString) {
		Enum category = getCategoryFromString(ExpenseCategory.class,
				categoryString);

		if (category != null)
			return category;

		category = getCategoryFromString(IncomeCategory.class, categoryString);

		if (category != null)
			return category;

		return null;
	}

	public static Class getCategoryTypeFromString(String categoryString) {

		Enum category = getCategoryFromString(categoryString);

		if (Arrays.asList(ExpenseCategory.values()).contains(category))
			return ExpenseCategory.class;

		if (Arrays.asList(IncomeCategory.values()).contains(category))
			return IncomeCategory.class;

		throw new IllegalArgumentException(
				"the passed argument cannot be found in known transaction categories !");
	}

	public static <T extends Enum> Class getCategoryType(T category) {
		if (category instanceof ExpenseCategory)
			return ExpenseCategory.class;

		if (category instanceof IncomeCategory)
			return IncomeCategory.class;

		throw new IllegalArgumentException("unknown catgory !");
	}

}
