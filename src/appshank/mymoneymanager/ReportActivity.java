package appshank.mymoneymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class ReportActivity extends Activity {

	private static final int DIALOG_ID_START_DATE = 0;
	private static final int DIALOG_ID_END_DATE = 1;

	TransactionsDatabaseHelper transactionsDataSource;
	TransactionCursorAdapter transactionCursorAdapter;
	// Cursor cursor;

	Date startDate, endDate;
	Calendar c;

	TextView startDateTextView;
	TextView endDateTextView;
	ListView transactionsListView;

	TextView textViewCurrencySymbolIncomes;
	TextView textViewCurrencySymbolExpenses;

	TextView totalIncomesView;
	TextView totalExpensesview;

	Handler handler;
	ProgressDialog progressDialog;

	private float totalIncomes;
	private float totalExpenses;

	// flag to set the text of the Filter button
	boolean filtersApplied = false;

	Button buttonFilter;
	ImageButton buttonViewAppliedFilters;
	ImageButton buttonRemoveAppliedFilters;

	ReportFilters filters;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("ReportActivity.onCreate", "inside onCreate() of ReportActivity");



		
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_report);


		handler = new Handler();

		// the database helper
		if (transactionsDataSource == null)
			transactionsDataSource = new TransactionsDatabaseHelper(this);

		transactionsDataSource.open();

		// initialize the references to the start and end date text view
		startDateTextView = (TextView) findViewById(R.id.start_date);
		endDateTextView = (TextView) findViewById(R.id.end_date);

		// button for filters
		buttonFilter = (Button) findViewById(R.id.button_add_filters);
		buttonViewAppliedFilters = (ImageButton) findViewById(R.id.button_view_applied_filters);
		buttonRemoveAppliedFilters = (ImageButton) findViewById(R.id.button_remove_applied_filters);

		// hide the view applied filters and remove applied filters button
		Utils.setVisibility(buttonViewAppliedFilters, false);
		Utils.setVisibility(buttonRemoveAppliedFilters, false);

		textViewCurrencySymbolIncomes = (TextView) findViewById(R.id.label_income_currency);
		textViewCurrencySymbolExpenses = (TextView) findViewById(R.id.label_expense_currency);
		textViewCurrencySymbolIncomes.setText(Utils.getCurrencySymbol());
		textViewCurrencySymbolExpenses.setText(Utils.getCurrencySymbol());

		totalIncomesView = (TextView) findViewById(R.id.textView_incomes_total);
		totalExpensesview = (TextView) findViewById(R.id.textView_expenses_total);

		// the dates
		c = Calendar.getInstance();

		if (savedInstanceState != null
				&& savedInstanceState.containsKey("rotation")
				&& savedInstanceState.getBoolean("rotation")) {
			Log.d("ReportActivity.onCreate()",
					"savedInstanceState contains 'rotation' set to 'true'");
			// set the end date
			endDate = new Date(savedInstanceState.getLong("end_date"));
			c.setTime(endDate);
			Utils.updateDateTextView(endDateTextView, c);
			// set the start date
			startDate = new Date(savedInstanceState.getLong("start_date"));
			c.setTime(startDate);
			Utils.updateDateTextView(startDateTextView, c);

		} else {
			if (savedInstanceState != null
					&& savedInstanceState.containsKey("rotation")
					&& savedInstanceState.getBoolean("rotation"))
				Log.d("ReportActivity.onCreate()",
						"savedInstanceState contains 'rotation' set to 'false'");

			// set the end date to be "today"
			endDate = new Date();
			c.setTime(endDate);
			Utils.updateDateTextView(endDateTextView, c);
			// set the start date to be the 1st of the current month
			c.set(Calendar.DAY_OF_MONTH, 1);
			c.set(Calendar.HOUR, 00);
			c.set(Calendar.MINUTE, 00);
			c.set(Calendar.SECOND, 01);
			startDate = c.getTime();
			Utils.updateDateTextView(startDateTextView, c);
		}

		if (savedInstanceState != null
				&& savedInstanceState.containsKey("are_filters_applied")
				&& savedInstanceState.getBoolean("are_filters_applied")) {
			Log.d("ReportActivity.onCreate()",
					"savedInstanceState contains 'are_filters_applied' with value 'true'");
			filtersApplied = true;

			Utils.setVisibility(buttonFilter, false);
			Utils.setVisibility(buttonViewAppliedFilters, true);
			Utils.setVisibility(buttonRemoveAppliedFilters, true);

			setFiltersFromSavedBundle(savedInstanceState);
			doStuffForFiltering();
		} else {

			// update the report list
			updateCursorAdapterView();

			// get the cursor for the transactions. iteratre over the
			// transactions
			// and calculate the total
			updateTotal();
		}

		// register listeners on start and end dates
		View.OnClickListener dateTextViewClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				onClickDate(v);
			}
		};
		startDateTextView.setOnClickListener(dateTextViewClickListener);
		endDateTextView.setOnClickListener(dateTextViewClickListener);
	}

	@Override
	protected void onStart() {
		super.onStart();

		EasyTracker.getInstance().activityStart(this);
	}

	/**
	 * adds the header to the listView
	 * 
	 * @param transactionsListView
	 * @param headerLayoutId
	 */
	private void addTransactionsListHeader(ListView transactionsListView,
			int headerLayoutId) {
		// get inflater
		LayoutInflater inflater = getLayoutInflater();
		// inflate header view according to the layout
		View transactionsListViewHeader = inflater.inflate(headerLayoutId,
				transactionsListView, false);
		// set the view as header
		transactionsListView.addHeaderView(transactionsListViewHeader);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_report, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickViewReport(View view) {

		Log.d("AppShank", "Generating report for Transactions between "
				+ startDate + " and " + endDate);

		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();

		// set a new cursor (as per start and end dates and filters) for the
		// cursoradapter
		if (filtersApplied == false) {
			Cursor newCursor = transactionsDataSource
					.getTransactionsBetweenDatesCursor(startDate, endDate);

			transactionCursorAdapter.changeCursor(newCursor);

			// check if update notification needs to be provided to the
			// cursorAdapter
			transactionCursorAdapter.notifyDataSetChanged();

			// update the total
			updateTotal();
		} else {
			filters.setDates(startDate, endDate);
			doStuffForFiltering();
		}

	}

	public void onClickFilter(View view) {
		// incase the filters are already applied, remove the filters
		if (filtersApplied) {
			filtersApplied = false;
			changeFilterButtonText();
			// hideFilters();

			// remove the filters and update the report
			onClickViewReport(view);
		} else {
			// start the filter activity with the correct request code
			Intent intent = new Intent(this, FilterActivity.class);
			startActivityForResult(intent, Constants.REQUEST_CODE_ADD_FILTERS);
		}
	}

	/*
	 * private void hideFilters() { // find the filters layout and hide it View
	 * filtersLayout = findViewById(R.id.layout_filter_values);
	 * filtersLayout.setVisibility(View.GONE); }
	 */

	private void updateTotal() {
		// take the cursor and calculate the sum of all the transactions
		if (transactionCursorAdapter.getCursor() != null) {
			// reinitialize the totalAmount
			Cursor cursor = transactionCursorAdapter.getCursor();
			totalIncomes = 0f;
			totalExpenses = 0f;
			if (cursor.moveToFirst()) {
				String type = "";
				do {
					type = cursor
							.getString(cursor
									.getColumnIndex(TransactionsDatabaseHelper.COLUMN_TRANSACTION_TYPE));
					float amount = cursor
							.getFloat(cursor
									.getColumnIndex(TransactionsDatabaseHelper.COLUMN_AMOUNT));
					if (type.equalsIgnoreCase(Transaction.TYPE_INCOME))
						totalIncomes += amount;
					else
						totalExpenses += amount;
				} while (cursor.moveToNext());
			}

			totalIncomesView.setText(Settings.format.format(totalIncomes));
			totalExpensesview.setText(Settings.format.format(totalExpenses));
		}

	}

	public void onClickStartDate(View view) {
		Log.d("AppShank", "inside onClickStartDate");
		onClickDate(view);
	}

	public void onClickEndDate(View view) {
		onClickDate(view);
	}

	@SuppressWarnings("deprecation")
	public void onClickDate(View view) {
		int dateDialogId = 0;
		switch (view.getId()) {
		case R.id.start_date:
			dateDialogId = DIALOG_ID_START_DATE;
			break;

		case R.id.end_date:
			dateDialogId = DIALOG_ID_END_DATE;
			break;
		}
		showDialog(dateDialogId);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ID_START_DATE:
			c.setTime(startDate);
			return new DatePickerDialog(this, new CustomOnDateSetListener(
					DIALOG_ID_START_DATE), c.get(Calendar.YEAR),
					c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		case DIALOG_ID_END_DATE:
			c.setTime(endDate);
			return new DatePickerDialog(this, new CustomOnDateSetListener(
					DIALOG_ID_END_DATE), c.get(Calendar.YEAR),
					c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DIALOG_ID_START_DATE:
			c.setTime(startDate);
			((DatePickerDialog) dialog).updateDate(c.get(Calendar.YEAR),
					c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			break;
		case DIALOG_ID_END_DATE:
			c.setTime(endDate);
			((DatePickerDialog) dialog).updateDate(c.get(Calendar.YEAR),
					c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			break;
		}
	}

	private class CustomOnDateSetListener implements OnDateSetListener {

		// this filed identifies wether the listener is for start date or end
		// date
		int dateTypeId;

		public CustomOnDateSetListener(int dateTypeId) {
			this.dateTypeId = dateTypeId;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			updateCalendar(year, monthOfYear, dayOfMonth);
			updateDate(dateTypeId);
			updateTextViewDate(dateTypeId);
		}

	}

	private void updateCalendar(int newYear, int newMonthOfYear,
			int newDayOfMonth) {
		c.set(Calendar.YEAR, newYear);
		c.set(Calendar.MONTH, newMonthOfYear);
		c.set(Calendar.DAY_OF_MONTH, newDayOfMonth);
	}

	private void updateDate(int dateTypeId) {
		switch (dateTypeId) {
		case DIALOG_ID_START_DATE:
			startDate = c.getTime();
			break;
		case DIALOG_ID_END_DATE:
			endDate = c.getTime();
			break;
		}
	}

	private void updateTextViewDate(int dateTypeId) {
		TextView dateView = null;
		switch (dateTypeId) {
		case DIALOG_ID_START_DATE:
			dateView = (TextView) findViewById(R.id.start_date);
			break;
		case DIALOG_ID_END_DATE:
			dateView = (TextView) findViewById(R.id.end_date);
			break;
		}

		Log.d("AppShank", "about to set the values in the dateView");

		if (dateView != null)
			dateView.setText(c.get(Calendar.DAY_OF_MONTH) + "/"
					+ (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();

		Log.d("ReportActivity.onActivityResult()", "requestCode: "
				+ requestCode);
		Log.d("ReportActivity.onActivityResult()", "resultCode: " + resultCode);

		if (requestCode == Constants.REQUEST_CODE_VIEW_TRANSACTION) {
			switch (resultCode) {
			case Constants.RESULT_CODE_TRANSACTION_UPDATED:
				// update the cursor and report
				updateCursorAdapterView();
				updateTotal();
				break;
			case Constants.RESULT_CODE_TRANSACTION_UPDATION_FAILED:
				// do nothing
				break;
			case Constants.RESULT_CODE_TRANSACTION_DELETED:
				// update the cursor and report
				updateCursorAdapterView();
				updateTotal();
				break;
			case Constants.RESULT_CODE_TRANSACTION_DELETION_FAILED:
				// do nothing
				break;
			default:
				break;
			}
		} else if (requestCode == Constants.REQUEST_CODE_ADD_FILTERS) {
			switch (resultCode) {
			case Constants.RESULT_CODE_FILTERS_SET:
				// filters were set
				filtersApplied = true;

				// hide the add filters button
				Utils.setVisibility(buttonFilter, false);

				// display the view applied filters and remove applied filters
				// buttons
				Utils.setVisibility(buttonViewAppliedFilters, true);
				Utils.setVisibility(buttonRemoveAppliedFilters, true);

				// find out which filters were set and get the updated cursor
				setFiltersFromIntent(data);
				doStuffForFiltering();

				break;
			case Constants.RESULT_CODE_FILTERS_NOT_SET:
				// do nothing
				break;
			default:
				break;
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setFiltersFromIntent(Intent data) {
		if (data.hasExtra("filters")) {
			filters = (ReportFilters) data.getSerializableExtra("filters");
			// update the dates of the filters object
			filters.setDates(startDate, endDate);
		}
	}

	private void setFiltersFromSavedBundle(Bundle savedInstanceState) {
		if (savedInstanceState.containsKey("are_filters_applied")) {
			if (filters == null)
				filters = new ReportFilters();

			String remarksContains = savedInstanceState
					.getString("filter_remarks_contains");
			if (remarksContains != null && remarksContains != ""
					&& remarksContains.length() != 0 && remarksContains != " ") {
				filters.setRemarksContains(remarksContains);
			}

			filters.setCategories(savedInstanceState
					.getStringArrayList("filter_categories"));

			float minAmount = savedInstanceState.getFloat("filter_min_amount",
					-1f);
			if (minAmount != -1f) {
				filters.setMinAmount(minAmount);
			}

			float maxAmount = savedInstanceState.getFloat("filter_max_amount",
					-1f);
			if (maxAmount != -1f) {
				filters.setMinAmount(maxAmount);
			}
		}
		// update the dates of the filters object
		filters.setDates(startDate, endDate);
	}

	private void doStuffForFiltering() {
		if (filters != null) {
			/*
			 * // show the layout_filters_applied and update its fields
			 * findViewById
			 * (R.id.layout_filter_values).setVisibility(View.VISIBLE);
			 * populateLayoutFiltersApplied(filters);
			 */
			// get the new cursor as per the filter values
			Cursor newCursor = getFilteredCursor(filters);

			// update the listview
			updateCursorAdapterView(newCursor);
			updateTotal();
		}
	}

	private void changeFilterButtonText() {
		if (filtersApplied)
			buttonFilter.setText("REMOVE FILTERS");
		else
			buttonFilter.setText(R.string.button_label_filter);

	}

	private void populateLayoutFiltersApplied(ReportFilters filters) {

		String remarksFilter = filters.isRemarksContainsSetFlag() ? filters
				.getRemarksContains() : "";
		String minAmountFilter = filters.isMinAmountSetFlag() ? Float
				.toString(filters.getMinAmount()) : "";
		String maxAmountFilter = filters.isMaxAmountSetFlag() ? Float
				.toString(filters.getMaxAmount()) : "";

		Log.d(getClass().getSimpleName() + ".populateLayoutFiltersApplied()",
				"Filters: " + "remarksFilter > " + remarksFilter
						+ " : minAmountFilter > " + minAmountFilter
						+ " : maxAmountFilter > " + maxAmountFilter);

		/*
		 * TextView textViewRemarksContains = (TextView)
		 * findViewById(R.id.textView_remarks_contains);
		 * textViewRemarksContains.setText(remarksFilter);
		 * 
		 * TextView textViewMinAmount = (TextView)
		 * findViewById(R.id.textView_min_amount);
		 * textViewMinAmount.setText(minAmountFilter);
		 * 
		 * TextView textViewMaxAmount = (TextView)
		 * findViewById(R.id.textView_max_amount);
		 * textViewMaxAmount.setText(maxAmountFilter);
		 */

	}

	private Cursor getFilteredCursor(ReportFilters filters) {
		return transactionsDataSource.getFilteredTransactions(filters);
	}

	/**
	 * updates the cursor due to an underlying data change (deletion, updation
	 * etc)
	 */
	private void updateCursorAdapterView() {
		// instantiate the adapter for transaction list
		Cursor cursor = transactionsDataSource
				.getTransactionsBetweenDatesCursor(startDate, endDate);
		updateCursorAdapterView(cursor);

	}

	private void updateCursorAdapterView(Cursor newCursor) {

		boolean isLanscape = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) ? true
				: false;
		Log.d(getClass().getSimpleName(), "orientation: "
				+ getResources().getConfiguration().orientation);
		if (transactionCursorAdapter == null) {
			Log.d("ReportActivity.updateCursorAdapterView()",
					"transactionCursorAdapter is null, reinitializing");
			transactionCursorAdapter = new TransactionCursorAdapter(this,
					newCursor, isLanscape);
		}

		transactionCursorAdapter.changeCursor(newCursor);

		// get the reference to the list view for displaying the transactions
		if (transactionsListView == null)
			transactionsListView = (ListView) findViewById(R.id.list_transaction);

		//
		if (transactionsListView.getAdapter() == null) {
			// add the header to the transactionsListView for the titles
			addTransactionsListHeader(transactionsListView,
					R.layout.report_listview_header);
		}

		// configure the list view to use the adapter
		transactionsListView.setAdapter(transactionCursorAdapter);
	}

	@Override
	protected void onResume() {
		Log.d("ReportActivity.onResume()", "inside onResume()");

		if (getIntent().hasExtra("deletion_successful"))
			Log.d("ReportActivity.onActivityResult()", ""
					+ getIntent().getExtras().keySet());

		// update the currency symbol
		textViewCurrencySymbolExpenses.setText(Utils.getCurrencySymbol() + " ");
		textViewCurrencySymbolIncomes.setText(Utils.getCurrencySymbol() + " ");

		// check the db
		if (!transactionsDataSource.isDatabaseOpen())
			transactionsDataSource.open();

		super.onResume();

	}

	public void onClickExport(View view) {
		// create a progress dialog to notify the user that the file for
		// exporting is being generated
		progressDialog = ProgressDialog.show(this, "Export Report",
				"Genearting file for exporting data...");

		// create a new thread to generate the xls file from the data in the
		// cursor
		Thread th = new Thread() {
			@Override
			public void run() {
				// instantiate the report generator
				ReportGenerator reportGenerator = new XLSReportGenerator(
						transactionCursorAdapter.getCursor());

				final String reportFilePath = reportGenerator.generateReport();
				Log.d("ReportActivity.onClickExport", "reportFilePath: "
						+ reportFilePath);

				// from within this new thread dismiss the progress dialog after
				// the xls
				// has been generated
				handler.post(new Runnable() {

					@Override
					public void run() {
						progressDialog.dismiss();

						// in cast reportFilePath is null, display toast to
						// notify user that creation of file failed
						if (reportFilePath == null) {
							handler.post(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(getApplicationContext(),
											"File Creation Failed !",
											Toast.LENGTH_SHORT).show();
								}
							});
						} else {
							// else create new intent and open the email to send
							shareFile(reportFilePath);
						}
					}

					private void shareFile(String reportFilePath) {
						Intent shareIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						shareIntent.setType("text/plain");
						shareIntent.putExtra(
								android.content.Intent.EXTRA_EMAIL,
								new String[] {});
						shareIntent.putExtra(
								android.content.Intent.EXTRA_SUBJECT,
								"MyMoneyManager - Transactions Report");
						shareIntent
								.putExtra(android.content.Intent.EXTRA_TEXT,
										"Generated via MyMoneyManager from AppShank...");
						shareIntent.putExtra(Intent.EXTRA_STREAM,
								Uri.parse("file://" + reportFilePath));
						startActivity(Intent.createChooser(shareIntent,
								"Send mail..."));
					}
				});
			}
		};
		th.start();

	}

	public void onClickViewAppliedFilters(View view) {
		// show the popup for applied filters
		showAppliedFiltersPopup(this);

		// populate the filters popup
	}

	private void showAppliedFiltersPopup(Activity context) {

		// set the popup's width and height
		// TEMP:
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int popupWidth = dm.widthPixels - 10;
		int popupHeight = dm.heightPixels - 60;

		// Inflate the popup_layout.xml
		LinearLayout viewGroup = (LinearLayout) context
				.findViewById(R.id.layout_applied_filters);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(R.layout.layout_filters_applied,
				viewGroup);

		// Creating the PopupWindow
		final PopupWindow popup = new PopupWindow(context);
		popup.setContentView(layout);
		popup.setWidth(popupWidth);
		popup.setHeight(popupHeight);
		popup.setFocusable(true);

		// Clear the default translucent background
		popup.setBackgroundDrawable(new BitmapDrawable());

		// Displaying the popup at the specified location
		popup.showAtLocation(layout, Gravity.CENTER, 0, 25);

		// disable the elements in this popup
		Utils.setupAppliedFiltersUI(layout, context);

		// populate the values in this popup window
		populateAppliedFiltersLayout(layout);

		// Getting a reference to Close button, and close the popup when
		// clicked.
		Button close = (Button) layout.findViewById(R.id.button_close);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popup.dismiss();
			}
		});
	}

	private void populateAppliedFiltersLayout(View layout) {
		// remarks
		if (filters.isRemarksContainsSetFlag()) {
			EditText remarksEditText = (EditText) layout
					.findViewById(R.id.editText_remarks_contains);
			remarksEditText.setText(filters.getRemarksContains());
		}

		// categories
		// hide the income and expense category layouts
		Utils.setVisibility(
				layout.findViewById(R.id.layout_all_expenses_categories), false);
		Utils.setVisibility(
				layout.findViewById(R.id.layout_all_incomes_categories), false);
		// populate selected categories scrollview
		if (filters.getCategories().size() != 0) {
			// iterate over the categories
			LinearLayout selectedCategoriesLayout = (LinearLayout) layout
					.findViewById(R.id.layout_selected_categories);
			List<String> selectedCategories = filters.getCategories();
			for (String categoryString : selectedCategories) {
				// populate the selected categories scrollview with the selected
				// category
				// add corresponding category to scrollview
				addToSelectedCategoriesScrollView(selectedCategoriesLayout,
						categoryString);
			}
			// update the category count
			updateCategoryCountTextView(layout, selectedCategories.size());
		}

		// min amount
		if (filters.isMinAmountSetFlag()) {
			EditText minAmountEditText = (EditText) layout
					.findViewById(R.id.editText_min_amount);
			minAmountEditText.setText(Float.toString(filters.getMinAmount()));
		}

		// max amount
		if (filters.isMaxAmountSetFlag()) {
			EditText maxAmountEditText = (EditText) layout
					.findViewById(R.id.editText_max_amount);
			maxAmountEditText.setText(Float.toString(filters.getMaxAmount()));
		}
	}

	private void updateCategoryCountTextView(View layout, int size) {
		TextView textViewCategoryCount = (TextView) layout
				.findViewById(R.id.textView_category_count);
		textViewCategoryCount.setText("(" + size + ")");
	}

	private void addToSelectedCategoriesScrollView(
			LinearLayout selectedCategoriesLayout, String selectedCategory) {
		// get the category corresponding to this string
		final Enum category = CategoryUtils
				.getCategoryFromString(selectedCategory);

		// get the set drawable corresponding to this category
		int drawableId = CategoryUtils.getCategorySetDrawable(category);

		// add an imageview to the scrollview
		ImageView imageView = new ImageView(this);

		// set the drawable for this imageview to the correct set drawable found
		// earlier
		imageView.setImageResource(drawableId);
		imageView.setTag(category.name());
		imageView.setAdjustViewBounds(true);
		int padding = Utils.getDPToPixels(this, 4);
		imageView.setPadding(padding, padding, padding, padding);

		int height = Utils.getDPToPixels(this, 50);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				height);

		selectedCategoriesLayout.addView(imageView, layoutParams);
	}

	public void onClickRemoveAppliedFilters(View view) {
		filtersApplied = false;

		// show the button for adding filters
		Utils.setVisibility(buttonFilter, true);
		changeFilterButtonText();

		// hide the buttons for viewing applied filters and removing applied
		// filters
		Utils.setVisibility(buttonViewAppliedFilters, false);
		Utils.setVisibility(buttonRemoveAppliedFilters, false);

		// display the toast according to the resultCode
		Utils.startToastService(this,
				Utils.getToastText(Constants.FILTERS_REMOVED));

		// remove the filters and update the report
		onClickViewReport(view);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d("ReportActivity.onSaveInstanceState()", "saving state");
		super.onSaveInstanceState(outState);

		// Save the values of the dates and filters here
		outState.putBoolean("rotation", true);
		outState.putLong("start_date", startDate.getTime());
		outState.putLong("end_date", endDate.getTime());
		outState.putBoolean("are_filters_applied", filtersApplied);
		if (filtersApplied) {
			if (filters != null) {
				// remarks
				if (filters.isRemarksContainsSetFlag()) {
					outState.putString("filter_remarks_contains",
							filters.getRemarksContains());
				}

				// categories
				outState.putStringArrayList("filter_categories",
						(ArrayList<String>) filters.getCategories());

				// min amount
				if (filters.isMinAmountSetFlag()) {
					outState.putFloat("filter_min_amount",
							filters.getMinAmount());
				}

				// max amount
				if (filters.isMaxAmountSetFlag()) {
					outState.putFloat("filter_max_amount",
							filters.getMaxAmount());
				}
			}
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onStop() {
		super.onStop();

		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	protected void onDestroy() {
		Log.d(getClass().getSimpleName(), "inside onDestroy()");

		// close the datasource
		if (transactionsDataSource != null) {
			transactionsDataSource.close();
		}

		super.onDestroy();
	}
}
