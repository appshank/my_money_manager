package appshank.mymoneymanager;

public class Constants {

	public static final int RESULT_CODE_TRANSACTION_UPDATED = 1;
	public static final int RESULT_CODE_TRANSACTION_UPDATION_FAILED = 2;
	public static final int RESULT_CODE_TRANSACTION_DELETED = 3;
	public static final int RESULT_CODE_TRANSACTION_DELETION_FAILED = 4;
	public static final int RESULT_CODE_TRANSACTION_SAVED = 5;
	public static final int RESULT_CODE_TRANSACTION_NOT_SAVED = 6;
	public static final int RESULT_CODE_FILTERS_SET = 7;
	public static final int RESULT_CODE_FILTERS_NOT_SET = 8;
	public static final int RESULT_MODIFICTION_SUCCESS = 9;

	public static final int REQUEST_CODE_ADD_TRANSACTION = 21;
	public static final int REQUEST_CODE_VIEW_TRANSACTION = 22;
	public static final int REQUEST_CODE_ADD_FILTERS = 23;

	public static final int FILTERS_REMOVED = 41;

	public static class ToastTexts {

	}
}
