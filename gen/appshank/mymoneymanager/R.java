/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package appshank.mymoneymanager;

public final class R {
    public static final class array {
        public static final int pref_countries_entries=0x7f070000;
        public static final int pref_countries_values=0x7f070001;
        public static final int pref_example_list_titles=0x7f070002;
        public static final int pref_example_list_values=0x7f070003;
        public static final int pref_sync_frequency_titles=0x7f070004;
        public static final int pref_sync_frequency_values=0x7f070005;
    }
    public static final class attr {
    }
    public static final class bool {
        /** Enable automatic activity tracking
         */
        public static final int ga_autoActivityTracking=0x7f060000;
        /** Enable automatic exception tracking
         */
        public static final int ga_reportUncaughtExceptions=0x7f060001;
    }
    public static final class color {
        public static final int expenseColor=0x7f080001;
        public static final int incomeColor=0x7f080002;
        public static final int sectionSeparatorColour=0x7f080000;
    }
    public static final class dimen {
        public static final int widget_margin=0x7f090000;
    }
    public static final class drawable {
        public static final int arrow_left=0x7f020000;
        public static final int arrow_right=0x7f020001;
        public static final int bill=0x7f020002;
        public static final int bill_selected=0x7f020003;
        public static final int books_n_stationery=0x7f020004;
        public static final int books_n_stationery_selected=0x7f020005;
        public static final int box=0x7f020006;
        public static final int box_not_pressed=0x7f020007;
        public static final int cinema_parties_n_outings=0x7f020008;
        public static final int cinema_parties_n_outings_selected=0x7f020009;
        public static final int cosmetics=0x7f02000a;
        public static final int cosmetics_selected=0x7f02000b;
        public static final int default_expense=0x7f02000c;
        public static final int default_expense_selected=0x7f02000d;
        public static final int default_income=0x7f02000e;
        public static final int default_income_selected=0x7f02000f;
        public static final int food=0x7f020010;
        public static final int food_selected=0x7f020011;
        public static final int fuel_n_conveyance=0x7f020012;
        public static final int fuel_n_conveyance_selected=0x7f020013;
        public static final int grocery=0x7f020014;
        public static final int grocery_selected=0x7f020015;
        public static final int ic_action_search=0x7f020016;
        public static final int ic_launcher=0x7f020017;
        public static final int interest_n_investments=0x7f020018;
        public static final int interest_n_investments_selected=0x7f020019;
        public static final int kids=0x7f02001a;
        public static final int kids_selected=0x7f02001b;
        public static final int lending_to_others=0x7f02001c;
        public static final int lending_to_others_selected=0x7f02001d;
        public static final int party_n_drinks=0x7f02001e;
        public static final int party_n_drinks_selected=0x7f02001f;
        public static final int payment_from_others=0x7f020020;
        public static final int payment_from_others_selected=0x7f020021;
        public static final int popup_bg=0x7f020022;
        public static final int popup_bg_white=0x7f020023;
        public static final int remove_filters=0x7f020024;
        public static final int rent_n_mortgage=0x7f020025;
        public static final int rent_n_mortgage_selected=0x7f020026;
        public static final int rounded_box=0x7f020027;
        public static final int rounded_box_large=0x7f020028;
        public static final int rounded_box_large_not_pressed=0x7f020029;
        public static final int rounded_box_large_pressed=0x7f02002a;
        public static final int rounded_box_not_pressed=0x7f02002b;
        public static final int rounded_box_pressed=0x7f02002c;
        public static final int salary=0x7f02002d;
        public static final int salary_selected=0x7f02002e;
        public static final int shopping=0x7f02002f;
        public static final int shopping_selected=0x7f020030;
        public static final int tuition=0x7f020031;
        public static final int tuition_selected=0x7f020032;
        public static final int vertical_line=0x7f020033;
        public static final int view_filters=0x7f020034;
        public static final int widget_bg=0x7f020035;
    }
    public static final class id {
        public static final int TableRow01=0x7f0c0043;
        public static final int above_total=0x7f0c0062;
        public static final int below_view_report_button=0x7f0c0031;
        public static final int button_add_filters=0x7f0c002e;
        public static final int button_add_transaction=0x7f0c0020;
        public static final int button_close=0x7f0c004f;
        public static final int button_delete_transaction=0x7f0c0049;
        public static final int button_done=0x7f0c001f;
        public static final int button_expenses=0x7f0c0019;
        public static final int button_export=0x7f0c002d;
        public static final int button_incomes=0x7f0c001d;
        public static final int button_remove_applied_filters=0x7f0c0030;
        public static final int button_selected_categories=0x7f0c0012;
        public static final int button_settings=0x7f0c0022;
        public static final int button_viewReport=0x7f0c002c;
        public static final int button_view_applied_filters=0x7f0c002f;
        public static final int button_view_report=0x7f0c0021;
        public static final int cat_bill=0x7f0c0053;
        public static final int cat_books_n_stationery=0x7f0c005a;
        public static final int cat_cinema_parties_n_outings=0x7f0c0057;
        public static final int cat_cosmetics=0x7f0c005b;
        public static final int cat_default_expense=0x7f0c005c;
        public static final int cat_default_income=0x7f0c0060;
        public static final int cat_food=0x7f0c0058;
        public static final int cat_fuel_n_conveyance=0x7f0c0051;
        public static final int cat_groceries=0x7f0c0054;
        public static final int cat_interest_n_investments=0x7f0c005e;
        public static final int cat_kids=0x7f0c0059;
        public static final int cat_party_n_drinks=0x7f0c0055;
        public static final int cat_payment_from_others=0x7f0c005f;
        public static final int cat_rent_n_mortage=0x7f0c0052;
        public static final int cat_salary=0x7f0c005d;
        public static final int cat_shopping=0x7f0c0056;
        public static final int checkBox_expense=0x7f0c000f;
        public static final int checkBox_income=0x7f0c000e;
        public static final int date_range=0x7f0c0033;
        public static final int dates_seperator=0x7f0c002a;
        public static final int editText_amount=0x7f0c003c;
        public static final int editText_date=0x7f0c003f;
        public static final int editText_max_amount=0x7f0c000b;
        public static final int editText_min_amount=0x7f0c0008;
        public static final int editText_remark=0x7f0c0042;
        public static final int editText_remarks_contains=0x7f0c0005;
        public static final int end_date=0x7f0c0029;
        public static final int imageButton1=0x7f0c0048;
        public static final int imageView_left_arrow=0x7f0c0013;
        public static final int imageView_right_arrow=0x7f0c0016;
        public static final int label_end_date=0x7f0c0028;
        public static final int label_expense_currency=0x7f0c0068;
        public static final int label_income_currency=0x7f0c0065;
        public static final int label_max_amount=0x7f0c000a;
        public static final int label_min_amount=0x7f0c0007;
        public static final int label_remarks_contains=0x7f0c0004;
        public static final int label_start_date=0x7f0c0026;
        public static final int label_total=0x7f0c0063;
        public static final int layout_add_transaction=0x7f0c0035;
        public static final int layout_all_expenses_categories=0x7f0c0017;
        public static final int layout_all_incomes_categories=0x7f0c001b;
        public static final int layout_applied_filters=0x7f0c004d;
        public static final int layout_categories=0x7f0c0050;
        public static final int layout_dates=0x7f0c0025;
        public static final int layout_expenses_categories=0x7f0c001a;
        public static final int layout_filters=0x7f0c0000;
        public static final int layout_incomes_categories=0x7f0c001e;
        public static final int layout_max_amount=0x7f0c0009;
        public static final int layout_min_amount=0x7f0c0006;
        public static final int layout_remarks_contains=0x7f0c0003;
        public static final int layout_select_category=0x7f0c000c;
        public static final int layout_selected_categories=0x7f0c0015;
        public static final int layout_view_export_buttons=0x7f0c002b;
        public static final int linearLayout1=0x7f0c0023;
        public static final int list_transaction=0x7f0c0061;
        public static final int menu_settings=0x7f0c0075;
        public static final int radioButton_expense=0x7f0c003a;
        public static final int radioButton_income=0x7f0c0039;
        public static final int radioGroup1=0x7f0c0038;
        public static final int scroll_date_range=0x7f0c0032;
        public static final int selected_categories_scrollView=0x7f0c0014;
        public static final int start_date=0x7f0c0027;
        public static final int tListItem_amount=0x7f0c0073;
        public static final int tListItem_category=0x7f0c0072;
        public static final int tListItem_date=0x7f0c0071;
        public static final int tListItem_s_no=0x7f0c0070;
        public static final int tableRow1=0x7f0c0037;
        public static final int tableRow2=0x7f0c003b;
        public static final int tableRow3=0x7f0c003d;
        public static final int tableRow4=0x7f0c0040;
        public static final int tableRow5=0x7f0c0046;
        public static final int tableRow6=0x7f0c004a;
        public static final int textView1=0x7f0c0010;
        public static final int textView3=0x7f0c0018;
        public static final int textView4=0x7f0c001c;
        public static final int textView_add_transaction_title=0x7f0c0036;
        public static final int textView_amount=0x7f0c0034;
        public static final int textView_bill=0x7f0c0047;
        public static final int textView_category=0x7f0c0044;
        public static final int textView_category_count=0x7f0c0011;
        public static final int textView_currency_symbol_expenses=0x7f0c006b;
        public static final int textView_currency_symbol_incomes=0x7f0c006e;
        public static final int textView_date_label=0x7f0c003e;
        public static final int textView_expenses_so_far=0x7f0c006c;
        public static final int textView_expenses_so_far_title=0x7f0c006a;
        public static final int textView_expenses_total=0x7f0c0069;
        public static final int textView_filter_transactions=0x7f0c0001;
        public static final int textView_filters_applied=0x7f0c004e;
        public static final int textView_incomes_so_far=0x7f0c006f;
        public static final int textView_incomes_so_far_title=0x7f0c006d;
        public static final int textView_incomes_total=0x7f0c0066;
        public static final int textView_label_expenses=0x7f0c0067;
        public static final int textView_label_income=0x7f0c0064;
        public static final int textView_remarks=0x7f0c0041;
        public static final int textView_selectedCategory=0x7f0c0045;
        public static final int textView_transaction_type=0x7f0c000d;
        public static final int textView_view_report=0x7f0c0024;
        public static final int title_seperator=0x7f0c0002;
        public static final int view_edit=0x7f0c0074;
        public static final int widget_add_expense_button=0x7f0c004c;
        public static final int widget_add_income_button=0x7f0c004b;
    }
    public static final class layout {
        public static final int activity_filter=0x7f030000;
        public static final int activity_main=0x7f030001;
        public static final int activity_report=0x7f030002;
        public static final int activity_transaction=0x7f030003;
        public static final int add_expense_appwidget=0x7f030004;
        public static final int layout_filters_applied=0x7f030005;
        public static final int layout_horizontalscrollview_categories_expenses=0x7f030006;
        public static final int layout_horizontalscrollview_categories_incomes=0x7f030007;
        public static final int layout_land_report=0x7f030008;
        public static final int layout_land_report_totals=0x7f030009;
        public static final int layout_main_incomes_expenses_this_month=0x7f03000a;
        public static final int layout_report=0x7f03000b;
        public static final int layout_report_totals=0x7f03000c;
        public static final int report_listview_header=0x7f03000d;
        public static final int transaction_list_item=0x7f03000e;
        public static final int util_horizontal_line_section=0x7f03000f;
        public static final int util_vertical_line_section=0x7f030010;
    }
    public static final class menu {
        public static final int activity_filter=0x7f0b0000;
        public static final int activity_main=0x7f0b0001;
        public static final int activity_report=0x7f0b0002;
        public static final int activity_transaction=0x7f0b0003;
    }
    public static final class string {
        public static final int app_name=0x7f050003;
        public static final int button_add_transaction=0x7f050009;
        public static final int button_edit=0x7f050018;
        public static final int button_filterActivity_apply_filters=0x7f050030;
        public static final int button_label_delete=0x7f050027;
        public static final int button_label_export=0x7f050024;
        public static final int button_label_filter=0x7f050028;
        public static final int button_save=0x7f050015;
        public static final int button_settings=0x7f05000b;
        public static final int button_update_table=0x7f050001;
        public static final int button_view_report=0x7f05000a;
        public static final int crash_report_text=0x7f050002;
        public static final int currency_symbol=0x7f050008;
        public static final int dummy_transaction_remark=0x7f050025;
        /** Replace placeholder ID with your tracking ID
         */
        public static final int ga_trackingId=0x7f050000;
        public static final int hello_world=0x7f050005;
        public static final int hint_enter_amount=0x7f050031;
        public static final int label_amount=0x7f050011;
        public static final int label_bill=0x7f050014;
        public static final int label_date=0x7f050012;
        public static final int label_end_date=0x7f050020;
        public static final int label_expense=0x7f050010;
        public static final int label_expenses_so_far=0x7f050006;
        public static final int label_filterActivity_maximum_amount=0x7f05002f;
        public static final int label_filterActivity_minimum_amount=0x7f05002e;
        public static final int label_filterActivity_remarks_contains=0x7f05002d;
        public static final int label_income=0x7f05000f;
        public static final int label_incomes_so_far=0x7f050007;
        public static final int label_maximum_amount=0x7f05002c;
        public static final int label_minimum_amount=0x7f05002b;
        public static final int label_remarks=0x7f050013;
        public static final int label_remarks_contains=0x7f05002a;
        public static final int label_start_date=0x7f05001f;
        public static final int label_total=0x7f050021;
        public static final int label_type_of_transaction=0x7f05000e;
        public static final int menu_settings=0x7f050004;
        public static final int pref_default_display_name=0x7f050038;
        public static final int pref_description_social_recommendations=0x7f050036;
        /**  Example settings for Data & Sync 
         */
        public static final int pref_header_data_sync=0x7f05003a;
        /**  Strings related to Settings 
 Example General settings 
         */
        public static final int pref_header_general=0x7f050034;
        /**  Example settings for Notifications 
         */
        public static final int pref_header_notifications=0x7f05003d;
        public static final int pref_ringtone_silent=0x7f050040;
        public static final int pref_title_add_friends_to_messages=0x7f050039;
        public static final int pref_title_display_name=0x7f050037;
        public static final int pref_title_new_message_notifications=0x7f05003e;
        public static final int pref_title_ringtone=0x7f05003f;
        public static final int pref_title_social_recommendations=0x7f050035;
        public static final int pref_title_sync_frequency=0x7f05003b;
        public static final int pref_title_system_sync_settings=0x7f05003c;
        public static final int pref_title_vibrate=0x7f050041;
        public static final int reportTitle_amount=0x7f05001c;
        public static final int reportTitle_category=0x7f05001e;
        public static final int reportTitle_date=0x7f05001a;
        public static final int reportTitle_i_e=0x7f05001b;
        public static final int reportTitle_remarks=0x7f050026;
        public static final int reportTitle_s_no=0x7f050019;
        public static final int reportTitle_view=0x7f05001d;
        public static final int temp_end_date=0x7f050023;
        public static final int temp_start_date=0x7f050022;
        public static final int title_activity_add_transaction=0x7f05000d;
        public static final int title_activity_filter=0x7f050029;
        public static final int title_activity_report=0x7f050016;
        public static final int title_activity_settings=0x7f050033;
        public static final int title_activity_transaction=0x7f05000c;
        public static final int title_filter_transactions=0x7f050032;
        public static final int title_view_report=0x7f050017;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f0a0000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f0a0001;
        public static final int activity_title=0x7f0a000a;
        public static final int amount_expense=0x7f0a0009;
        public static final int amount_income=0x7f0a0008;
        public static final int baseTextStyle=0x7f0a000d;
        public static final int button_report_activity=0x7f0a0004;
        /**  Large End 
         */
        public static final int dateLabelStyle=0x7f0a001d;
        public static final int editText_filter_amount=0x7f0a0006;
        public static final int filtersApplied_labels=0x7f0a0007;
        public static final int label_filters=0x7f0a0005;
        public static final int large_activity_title=0x7f0a0017;
        public static final int large_amount_expense=0x7f0a0016;
        public static final int large_amount_income=0x7f0a0015;
        public static final int large_baseTextStyle=0x7f0a001a;
        public static final int large_button_report_activity=0x7f0a0011;
        public static final int large_dateLabelStyle=0x7f0a001e;
        public static final int large_editText_filter_amount=0x7f0a0013;
        public static final int large_filtersApplied_labels=0x7f0a0014;
        public static final int large_label_filters=0x7f0a0012;
        /**  Normal Size End 
 Large Start 
         */
        public static final int large_report_header=0x7f0a000f;
        public static final int large_report_list_item=0x7f0a0010;
        public static final int large_style_cat_icons=0x7f0a0018;
        public static final int large_style_report_date=0x7f0a0019;
        public static final int large_textViewStyle_main_large=0x7f0a001b;
        public static final int large_transactionActivityLabel=0x7f0a001c;
        /**  Normal Size 
         */
        public static final int report_header=0x7f0a0002;
        public static final int report_list_item=0x7f0a0003;
        public static final int style_cat_icons=0x7f0a000b;
        public static final int style_report_date=0x7f0a000c;
        public static final int transactionActivityLabel=0x7f0a000e;
    }
    public static final class xml {
        public static final int add_expense_widget_info=0x7f040000;
        public static final int pref_data_sync=0x7f040001;
        public static final int pref_general=0x7f040002;
        public static final int pref_headers=0x7f040003;
        public static final int pref_notification=0x7f040004;
        public static final int pref_settings=0x7f040005;
    }
}
